﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Yurowm.Utilities;

namespace Yurowm.UI {
    public static class MaskReferences {

        static readonly Dictionary<string, Func<ReferenceData>> references = new Dictionary<string, Func<ReferenceData>>();
        static List<string> keys = null;
        static bool isInitialized = false;
        
        [OnLaunch(-1000)]
        static void Initialize() {
            if (!Utils.IsMainThread() && Application.isPlaying)
                return;
            
            var parameters = new object[0];
            foreach (var pair in Utils.GetAllMethodsWithAttribute<MaskReferenceAttribute>
                (BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                .OrderBy(p => p.Item2.name))
                references[pair.Item2.name] = () => {
                    var method = pair.Item1 as MethodInfo;
                    ReferenceData data = new ReferenceData {
                        type = method.ReturnType,
                        name = pair.Item2.name
                    };
                    try {
                        data.value = method.Invoke(null, parameters);
                    } catch {}
                    return data;
                };
                
            keys = references.Keys.ToList();
            
            isInitialized = true;
        }

        public static object Get(string key) {
            return references.ContainsKey(key) ? references[key]().value : 0;
        }
        
        public static Type GetType(string key) {
            return references.ContainsKey(key) ? references[key]().type : null;
        }
        
        public static List<ReferenceData> Keys() {
            if (!isInitialized) Initialize();
            return references.Values.Select(v => v.Invoke()).ToList();
        }

        public struct ReferenceData {
            public Type type;
            public string name;
            public object value;
        }
    }
    
    public class MaskReferenceAttribute : Attribute {
        public readonly string name;
        
        public MaskReferenceAttribute(string name) {
            this.name = name;
        }
        
    }
}