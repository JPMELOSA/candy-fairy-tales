﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using System.Linq;
using Yurowm.Coroutines;
using Yurowm.Extensions;
using Yurowm.Spaces;
using Yurowm.Utilities;

namespace Yurowm {
    [ExecuteAlways]
    public class ContentAnimator : BaseBehaviour, SpaceTime.ISensitiveComponent, IClipComponent {

        public bool ignoreTimeScale;
        
        State state = null;

        [HideInInspector]
        public List<Clip> clips = new List<Clip>();

        public bool GetClip(string name, out Clip clip) {
            clip = clips.FirstOrDefault(x => x.name == name);
            return clip != null && clip.clip != null;
        }

        public void Play(string clipName) {        
            if (GetClip(clipName, out var clip)) 
                state = new State(clip);
        }

        public void Play(string clipName, WrapMode wrapMode) {        
            if (GetClip(clipName, out var clip))
                state = new State(clip, wrapMode);
        }

        public void Play(string clipName, WrapMode wrapMode, float timeScale, float timeOffset) {        
            if (GetClip(clipName, out var clip)) {
                state = new State(clip, wrapMode);
                state.Time = timeOffset.Repeat(1);
                state.timeScale = timeScale;
            } 
        }

        public IEnumerator WaitPlaying() {
            while (IsPlaying() && gameObject.activeInHierarchy)
                yield return null;
        }

        public IEnumerator PlayAndWait(string clipName) {
            if (GetClip(clipName, out var clip)) {
                state = new State(clip);
                return WaitPlaying();
            }

            return null;
        }
        
        public bool HasClip(string clipName) {
            return GetClip(clipName, out _);
        }

        public void Stop(string clipName) {
            if (IsPlaying(clipName))
                Stop();
        }
        
        public void Stop() {
            state = null;
        }

        public void Complete(string clipName) {
            if (IsPlaying(clipName))
                state.mode = WrapMode.Once;
        }

        public void CompleteAndPlay(string clipName) {
            if (IsPlaying(clipName)) {
                state.mode = WrapMode.Once;
                WaitPlaying()
                    .ContinueWith(() => Play(clipName))
                    .Run();
            } else
                Play(clipName);
        }

        public bool IsPlaying(string clipName) {
            return state != null && state.clip.name == clipName;
        }
        
        public bool IsPlaying() {
            #if UNITY_EDITOR
            if (!Application.isPlaying)
                return true;
            #endif
            
            return state != null;
        }

        public void Rewind(string clipName) {
            if (GetClip(clipName, out var clip)) {
                state = new State(clip);
                state.timeScale = 0;
                Sample();
            }
        }

        public void RewindEnd(string clipName) {
            if (GetClip(clipName, out var clip)) {
                state = new State(clip);
                state.Time = 1;
                state.timeScale = 0;
                Sample();
            }
        }
        
        void Update() {
            Sample();
        }
        
        SpaceTime time = null;

        public void OnChangeTime(SpaceTime time) {
            this.time = time;
        }
        
        void Sample() {
            if (!enabled) return;
            if (state != null) {
                if (!state.Next(gameObject, time?.Delta ?? (ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime)))
                    state = null;
                SendOnAnimate();
            }
        }
        
        IOnAnimateHandler[] onAnimateHandlersCache;

        void LateUpdate() {
            if (!enabled) return;
            if (IsPlaying()) {
                if (onAnimateHandlersCache == null)
                    onAnimateHandlersCache = gameObject.GetComponentsInChildren<IOnAnimateHandler>();
                SendOnAnimate();
            } else
                onAnimateHandlersCache = null;
        }
        
        public void SendOnAnimate() {
            (onAnimateHandlersCache ?? gameObject.GetComponentsInChildren<IOnAnimateHandler>())
                .ForEach(h => h.OnAnimate());
        }

        [Serializable]
        public class Clip {
            public string name;
            public bool reverse = false;
            public AnimationClip clip;

            public Clip(string name) {
                this.name = name;
            }
        }
        
        class State {
            public Clip clip;
            
            float time = 0;
            public float Time {
                get => (clip.reverse ? length - time : time) / length;
                set => time = (clip.reverse ? 1f - value : value).Clamp01() * length;
            }
            
            public float timeScale;
            public WrapMode mode;
            
            float length;
            bool complete = false;

            public State(Clip clip) : this(clip, clip.clip.wrapMode) {}

            public State(Clip clip, WrapMode wrapMode) {
                this.clip = clip;
                length = clip.clip.length;
                mode = wrapMode;
                timeScale = 1;
                Time = 0;
            }

            public bool Next(GameObject gameObject, float deltaTime) {
                if (complete)
                    return false;

                if (this.clip.reverse)
                    deltaTime *= -1;
                
                time += deltaTime * timeScale;
                
                var clip = this.clip.clip;

                switch (mode) {
                    case WrapMode.Default: 
                    case WrapMode.Once: {
                        if (time < 0 || time > length) {
                            complete = true;
                            time = time.Clamp(0, length);
                        }
                        clip.SampleAnimation(gameObject, time);
                        break;
                    }
                    case WrapMode.PingPong:
                        clip.SampleAnimation(gameObject, Mathf.PingPong(time, clip.length));
                        break;
                    case WrapMode.Loop: {
                        time = Mathf.Repeat(time, clip.length);
                        clip.SampleAnimation(gameObject, time);
                        break;
                    }
                }
                
                if (timeScale == 0)
                    complete = true;
                
                return !complete;
            }
        }
    }
    
    public interface IOnAnimateHandler {
        void OnAnimate();
    }
    
}
