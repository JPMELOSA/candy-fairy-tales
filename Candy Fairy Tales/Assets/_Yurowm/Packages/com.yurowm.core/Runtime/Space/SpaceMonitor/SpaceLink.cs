﻿using System.Linq;

namespace Yurowm.Spaces {
    public class SpaceLink : Behaviour {

        [TypeSelector.Target(typeof (Space))]
        public TypeSelector spaceType;

        public Space space = null;
        
        void OnEnable() {
            Space.Show(spaceType.GetSelectedType(), s => space = s);
        }

        public override void OnKill() {
            if (space) {
                space.Destroy();
                space = null;
            }
        }

        void OnDisable() {
            Space.Hide(spaceType.GetSelectedType());
        }
        
        public void Pause() {
            Space.all
                .FirstOrDefault(s => spaceType.GetSelectedType().IsInstanceOfType(s))?
                .Pause();
        }
        
        public void Unpause() {
            Space.all
                .FirstOrDefault(s => spaceType.GetSelectedType().IsInstanceOfType(s))?
                .Unpause();
        }
    }
}