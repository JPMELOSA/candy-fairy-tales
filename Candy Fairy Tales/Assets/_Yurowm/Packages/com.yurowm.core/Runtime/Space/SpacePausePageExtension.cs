﻿using System;
using System.Linq;
using Yurowm.Extensions;
using Yurowm.Serialization;
using Yurowm.UI;

namespace Yurowm.Spaces {
    public class SpacePausePageExtension : PageExtension {
        public string spaceType;
        public bool pause;
        
        public override void OnShow() {
            base.OnShow();
            if (spaceType.IsNullOrEmpty()) return;
            
            var type = Type.GetType(spaceType);
            if (type == null) return;
            
            var space = Space.all.FirstOrDefault(s => type.IsInstanceOfType(s));

            if (space == null) return;
            
            if (pause)
                space.Pause();
            else
                space.Unpause();
        }

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("spaceType", spaceType);
            writer.Write("pause", pause);
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("spaceType", out spaceType);
            reader.Read("pause", out pause);
        }
    }
}