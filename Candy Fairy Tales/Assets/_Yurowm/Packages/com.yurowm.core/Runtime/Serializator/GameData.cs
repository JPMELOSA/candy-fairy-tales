﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Yurowm.Coroutines;
using Yurowm.Extensions;
using Yurowm.InUnityReporting;
using Yurowm.Utilities;

namespace Yurowm.Serialization {
    public class GameData : ISerializable {
        
        CryptKey Key;
        
        #region Modules

        List<Module> modules = new List<Module>();
        
        public M GetModule<M>() where M : Module {
            return modules.CastOne<M>() ?? CreateModule<M>();
        }
        
        public bool CreateModule<M>(out M module) where M : Module {
            module = CreateModule<M>();
            return module != null;
        }
        
        public M CreateModule<M>() where M : Module {
            var result = modules.CastOne<M>();
            
            if (result != null)
                return null;

            try {
                result = Activator.CreateInstance<M>();
                result.setDirty = SetDirty;
                result.Initialize();
                modules.Add(result);
            } catch (Exception e) {
                Debug.LogException(e);
                result = null;
            }
            
            return result;
        }
        
        public abstract class Module : ISerializable {
            public Action setDirty;
        
            public void SetDirty() {
                setDirty.Invoke();    
            }
        
            public virtual void Initialize() {}
    
            public abstract void Serialize(Writer writer);

            public abstract void Deserialize(Reader reader);
        }
        
        #endregion
        
        readonly string fileName;
        
        public GameData(string name, string cryptKey = null) {
            fileName = name + Serializator.FileExtension;
            Key = cryptKey.IsNullOrEmpty() ? null : CryptKey.Get(cryptKey);
        }
        
        #region Save & Load
        
        bool isDirty;
        
        public void SetDirty() {
            isDirty = true;
        }

        IEnumerator DirtyCheck() {
            while (true) {
                if (isDirty) {
                    Save();
                    yield return new Wait(1);
                }
                yield return null;
            }
        }
        
        IEnumerator ReadFromFile() {
            string raw = null;
            
            yield return TextData.LoadTextProcess(
                Path.Combine("Data", fileName),
                r => raw = r,
                TextCatalog.Persistent);
            
            if (raw.IsNullOrEmpty())
                yield break;
            
            if (Key != null)
                raw = raw.Decrypt(Key);
            
            Serializator.FromTextData(this, raw);
        }

        void Save() {
            isDirty = false;
            
            string raw = Serializator.ToTextData(this);
            
            if (Key != null)
                raw = raw.Encrypt(Key);
            
            TextData.SaveText(
                Path.Combine("Data", fileName),
                raw,
                TextCatalog.Persistent);
        }

        public IEnumerator Load() {
            yield return ReadFromFile();
            Initialize();
        }
        
        void Initialize() {
            modules.ForEach(m => m.Initialize());
            DirtyCheck().Run();
            Reporter.AddReport($"Game Data ({fileName})", new SerializableReport(this));
        }
        
        public void Clear() {
            modules.Clear();
            SetDirty();
        }   
        
        #endregion
        
        #region ISerializable
        
        public void Serialize(Writer writer) {
            writer.Write("modules", modules.ToArray());
        }

        public void Deserialize(Reader reader) {
            modules.Clear();
            modules.AddRange(reader.ReadCollection<Module>("modules"));
            modules.ForEach(m => m.setDirty = SetDirty);
        }
        
        #endregion
    }
}