﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Yurowm.Dashboard;
using Yurowm.Extensions;
using Yurowm.GUIHelpers;
using Yurowm.GUIStyles;
using Yurowm.Icons;
using Yurowm.ObjectEditors;
using Yurowm.Serialization;

namespace Yurowm.DeveloperTools {
    [DashboardGroup("Development")]
    [DashboardTab("Optimizations", "Hammer")]
    public class OptimizationStorageEditor : StorageEditor<Optimization> {
        bool allowToBuild = false;
        
        static Texture2D statusIcon;

        public override string GetItemName(Optimization item) {
            return item.ID;
        }

        public override Storage<Optimization> OpenStorage() {
            return Optimization.storage;
        }

        protected override void Sort() {}

        public override void OnGUI() {
            if (statusIcon == null) statusIcon = EditorIcons.GetIcon("Dot");
            base.OnGUI();
        }

        public override void OnToolbarGUI() {
            base.OnToolbarGUI();
            if (GUILayout.Button("Analysis", EditorStyles.toolbarButton, GUILayout.Width(100)))
                Analysis();
            if (allowToBuild && GUILayout.Button("Build", EditorStyles.toolbarButton, GUILayout.Width(100))) {
                Analysis();
                if (allowToBuild)
                    PackageExporter.Export(PackageExporter.PassType.Customer);
            }
        }

        public void Analysis() {
            var title = "Release Optimizations";
            
            EditorUtility.DisplayProgressBar(title, "Reflection", 0);

            var optimizations = storage.items.ToArray();
            
            float i = 0;
            foreach (var optimization in optimizations) {
                EditorUtility.DisplayProgressBar(title, optimization.ID, i / optimizations.Length);
                optimization.Analysis();
                i++;
            }

            EditorUtility.ClearProgressBar();
            
            allowToBuild = optimizations.All(t => t.validation == Optimization.Validation.Passed);
        }

        protected override Rect DrawItem(Rect rect, Optimization item) {
            rect = ItemIconDrawer.Draw(rect, statusIcon, item.GetColor());
            return base.DrawItem(rect, item);
        }
    }

    public abstract class Optimization : ISerializableID {
        
        public static Storage<Optimization> storage = new Storage<Optimization>("Optimization", TextCatalog.ProjectFolder);
     
        public string ID { get; set; }
        
        public string report;

        public enum Validation {
            Unknown,
            Passed,
            NotPassed,
            Error
        }

        static readonly Color unknownColor = new Color(0.76f, 1f, 1f);
        static readonly Color passedColor = new Color(.5f, 1f, .5f);
        static readonly Color notPassedColor = new Color(0.6f, 0.44f, 0.44f);
        static readonly Color errorColor = new Color(1f, 0.24f, 0.29f);

        public Validation validation = Validation.Unknown;
        
        public void Analysis() {
            report = "";
            
            if (!isInitialized)
                Initialize();
            
            try {
                validation = DoAnalysis() ? Validation.Passed : Validation.NotPassed;
            }
            catch (Exception e) {
                Debug.LogException(e);
                validation = Validation.Error;
            }
        }
        
        public abstract bool DoAnalysis();
        public abstract bool CanBeAutomaticallyFixed();
        public virtual void Fix() {}
        
        bool isInitialized = false;
        
        public void Initialize() {
            if (isInitialized)
                return;
            isInitialized = true;
            OnInitialize();
        }
        
        public virtual void OnInitialize() { }

        public Color GetColor() {
            switch (validation) {
                case Validation.Unknown: return unknownColor;
                case Validation.Passed: return passedColor;
                case Validation.NotPassed: return notPassedColor;
                case Validation.Error: return errorColor;
                default: return EditorGUIUtility.isProSkin ? Color.white : Color.black;
            }
        }

        public virtual void Serialize(Writer writer) {
            writer.Write("ID", ID);
        }

        public virtual void Deserialize(Reader reader) {
            ID = reader.Read<string>("ID");
        }
    }
    
    public class ReleaseOptimizationEditor : ObjectEditor<Optimization> {
        public override void OnGUI(Optimization ro, object context = null) {
            ro.Initialize();
            
            using (GUIHelper.Horizontal.Start()) {
                EditorGUILayout.PrefixLabel("Status");
                using (GUIHelper.Color.Start(ro.GetColor())) {
                    GUILayout.Label(ro.validation.ToString(), Styles.whiteBoldLabel);
                    
                    if (GUILayout.Button("Analysis", EditorStyles.miniButton, GUILayout.Width(80)))
                        ro.Analysis();

                    if (ro.validation == Optimization.Validation.NotPassed && ro.CanBeAutomaticallyFixed()) {
                        if (GUILayout.Button("Fix", EditorStyles.miniButton, GUILayout.Width(40))) {
                            ro.Fix();
                            if (context is OptimizationStorageEditor rose)
                                rose.Analysis();
                        }
                    }
                }
            }
            
            if (!ro.report.IsNullOrEmpty())
                EditorGUILayout.HelpBox(ro.report, MessageType.None, false);

        }
    }
}