﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using Yurowm.Coroutines;
using Yurowm.Dashboard;
using Yurowm.Extensions;
using Yurowm.GUIHelpers;
using Yurowm.GUIStyles;
using Yurowm.HierarchyLists;
using Yurowm.Icons;
using Yurowm.InUnityReporting;
using Yurowm.ObjectEditors;
using Yurowm.Utilities;

namespace Yurowm.Serialization {
    public abstract class StorageEditor<S> : DashboardEditor where S : class, ISerializable {
        protected Storage<S> storage { get; private set; }
        ItemsList list;
        List<S> selected = new List<S>();

        GUIHelper.LayoutSplitter splitter;
        GUIHelper.Scroll settingsScroll = new GUIHelper.Scroll(GUILayout.ExpandHeight(true));
        GUIHelper.SearchPanel searchPanel = new GUIHelper.SearchPanel("");

        public override bool isScrollable => false;

        bool rawView = false;

        Dictionary<S, SerializableReportEditor> rawViewDict = new Dictionary<S, SerializableReportEditor>();
        
        static Texture2D textIcon;
        static Texture2D saveIcon;
        static Texture2D menuIcon;
        static Texture2D defaultIcon;
        static Texture2D debugIcon;
        static Texture2D releaseIcon;

        #region New Item
        protected virtual bool CanCreateNewItem() {
            return true;
        }

        S newItem;
        public void CreateNewItem(Type type) {
            if (!typeof(S).IsAssignableFrom(type)) return;
            CreateNewItem((S) Activator.CreateInstance(type));
        }
        
        public void CreateNewItem(S item) {
            if (item == null) return;
            
            try {
                newItem = item;
                SetupNewItem(newItem);
                var popup = DashboardPopup.Create<NewItemPopup>();
                popup.Setup(newItem.GetType().Name.NameFormat(), OnNewItemGUI);
                (window as YDashboard)?.ShowPopup(popup);
            } catch (Exception e) {
                newItem = null;
                UnityEngine.Debug.LogException(e);
            }
        }

        protected void DuplicateNewItem(S original) {
            if (original == null) return;
            CreateNewItem(original.Clone());
        }

        protected void AddItem(S item) {
            storage.items.Add(item);
            Sort();
            list.Reload();
            
            selected.Clear();
            selected.Add(item);
        }

        bool OnNewItemGUI() {
            if (newItem == null) return false;
            
            if (newItem is ISerializableID sid)
                sid.ID = EditorGUILayout.TextField("ID", sid.ID);
            
            ObjectEditor.Edit(newItem);
            
            GUILayout.FlexibleSpace();

            using (GUIHelper.Lock.Start(!FilterNewItem(newItem))) {
                if (GUILayout.Button("Create")) {
                    AddItem(newItem);
                    newItem = null;
                    return false;
                }
            }
            return true;
        }

        protected virtual bool FilterNewItem(S item) {
            if (item is ISerializableID sid) {
                var id = sid.ID;
                return !id.IsNullOrEmpty() && storage.items
                           .CastIfPossible<ISerializableID>()
                           .All(i => i.ID != id);
            }
            return true;
        }
        
        public virtual void SetupNewItem(S item) {
            if (item is ISerializableID sid && sid.ID.IsNullOrEmpty())
                sid.ID = item.GetType().Name;
        }
        
        #endregion

        public abstract string GetItemName(S item);
        
        public virtual string GetTitle(S item) {
            return GetItemName(item);
        }
        
        public virtual string GetFolderName(S content) {
            return "";
        }
        
        Rect DrawItemInternal(Rect rect, S item) {
            if (Event.current.type != EventType.Repaint) return default;
            
            rect = ItemIconDrawer.DrawAuto(rect, item);
            
            if (item is IStorageElementExtraData extraData) {
                if (extraData.storageElementFlags.HasFlag(StorageElementFlags.WorkInProgress))
                    Handles.DrawSolidRectangleWithOutline(rect, Color.yellow.Transparent(.1f), Color.clear);
                    
                if (extraData.storageElementFlags.HasFlag(StorageElementFlags.DefaultElement))
                    rect = ItemIconDrawer.DrawSolid(rect, defaultIcon);
                
                if (extraData.storageElementFlags.HasFlag(StorageElementFlags.DebugOnly))
                    rect = ItemIconDrawer.DrawSolid(rect, debugIcon);
                
                if (extraData.storageElementFlags.HasFlag(StorageElementFlags.ReleaseOnly))
                    rect = ItemIconDrawer.DrawSolid(rect, releaseIcon);
            }

            return DrawItem(rect, item);
        }

        protected virtual Rect DrawItem(Rect rect, S item) {
            return rect;
        }

        public abstract Storage<S> OpenStorage();

        public override bool Initialize() {
            storage = OpenStorage();
            
            Sort();
            
            rawViewDict.Clear();
            
            bool grooped = GetType().GetMethod("GetFolderName")?.DeclaringType == GetType();
            
            list = new ItemsList(storage.items, grooped ? GetFolderName : (Func<S, string>) null, GetItemName);
            list.onSelectedItemChanged += s => selected = s;
            list.types = Utils
                .FindInheritorTypes<S>(false)
                .Where(t => !t.IsAbstract)
                .OrderBy(t => t.FullName)
                .ToArray();
            list.newItem = CreateNewItem;
            list.duplicateItem = DuplicateNewItem;
            list.onContextMenu += OnItemsContextMenu;
            list.drawItem = DrawItemInternal;
            
            splitter = new GUIHelper.LayoutSplitter(OrientationLine.Horizontal, OrientationLine.Vertical, 250f);

            return true;
        }

        public override void OnGUI() {
            if (textIcon == null) textIcon = EditorIcons.GetUnityIcon("d_UnityEditor.ConsoleWindow@2x");
            if (saveIcon == null) saveIcon = EditorIcons.GetUnityIcon("d_SaveAs@2x");
            if (menuIcon == null) menuIcon = EditorIcons.GetUnityIcon("d__Menu@2x");
            if (defaultIcon == null) defaultIcon = EditorIcons.GetIcon("Default");
            if (debugIcon == null) debugIcon = EditorIcons.GetIcon("NoIcon");
            if (releaseIcon == null) releaseIcon = EditorIcons.GetIcon("YesIcon");
            
            using (splitter.Start(true, true)) {
                if (splitter.Area())
                    list.OnGUI();
                if (splitter.Area()) {
                    using (settingsScroll.Start()) {
                        if (selected != null) {
                            foreach (var item in selected.Take(10)) {
                                if (item == null) return;
                                GUILayout.Label(GetTitle(item), Styles.title);
                                GUILayout.Label($"<{item.GetType().FullName}>", Styles.miniLabelBlack);
                                if (rawView)
                                    DrawRawItem(item);
                                else {
                                    ObjectEditor.CopyPaste("Data", item);
                                    ObjectEditor.Edit(item, this);
                                }
                            }
                            GUILayout.FlexibleSpace();
                        }
                    }
                }
            }
        }
        
        void DrawRawItem(S item) {
            if (!rawViewDict.TryGetValue(item, out var editor)) {
                editor = new SerializableReportEditor();
                var report = new SerializableReport(item);
                report.Refresh();
                editor.SetProvider(report);
                rawViewDict.Add(item, editor);
            }
            
            editor.OnGUI(GUILayout.Height(Mathf.Min(500, editor.TreeHeight)));
        }

        public virtual void OnStorageToolbarGUI() {}

        public override void OnToolbarGUI() {
            using (GUIHelper.Horizontal.Start(EditorStyles.toolbar, GUILayout.ExpandWidth(true))) {
                using (GUIHelper.Color.ProLiteStart()) {
                    if (rawView != GUILayout.Toggle(rawView, textIcon, EditorStyles.toolbarButton, GUILayout.Width(30))) {
                        rawView = !rawView;
                        if (rawView) rawViewDict.Values.ForEach(e => e.Refresh());
                    }
                }

                OnStorageToolbarGUI();
                
                searchPanel.OnGUI(list.SetSearchFilter, GUILayout.ExpandWidth(true));

                using (GUIHelper.Color.ProLiteStart()) {
                    if (GUILayout.Button(saveIcon, EditorStyles.toolbarButton, GUILayout.Width(30)))
                        Apply();
                    if (GUILayout.Button(menuIcon, EditorStyles.toolbarButton, GUILayout.Width(30))) {
                        var menu = new GenericMenu();
                        OnOtherContextMenu(menu);
                        if (menu.GetItemCount() > 0)
                            menu.ShowAsContext();
                    }
                }
            }
        }
        
        protected virtual void OnOtherContextMenu(GenericMenu menu) {
            menu.AddItem(new GUIContent("Reset"), false, () => {
                storage.LoadFast();
                Sort();
                list.itemCollection = storage.items;
                list.Reload();
            });
            
            menu.AddItem(new GUIContent("Raw Data/Save to System Buffer"), false, () => {
                EditorGUIUtility.systemCopyBuffer = Serializator.ToTextData(storage);
            });
            
            menu.AddItem(new GUIContent("Raw Data/Source to System Buffer"), false, () => {
                storage.GetSource(r => EditorGUIUtility.systemCopyBuffer = r)
                    .Complete()
                    .Run();
            });
            
            menu.AddItem(new GUIContent("Raw Data/Inject"), false, () => {
                try {
                    var raw = EditorGUIUtility.systemCopyBuffer;
                    var reference = Serializator.FromTextData(raw);
                    if (reference != null && reference.GetType() == storage.GetType()) {
                        Serializator.FromTextData(storage, raw);
                        Sort();
                        list.itemCollection = storage.items;
                        list.Reload();
                        UnityEngine.Debug.Log("Successfully Injected");
                    }
                } catch (Exception e) {
                    UnityEngine.Debug.LogException(e);
                }
            });
        }
        
        protected virtual void Sort() {
            storage.items.Sort(GetItemName);
        }
        
        public virtual void Apply() {
            storage.Apply();
        }
        
        protected virtual void OnItemsContextMenu(GenericMenu menu, S[] items) { }
        
        public class ItemsList : HierarchyList<S> {
            List<IInfo> selected = new List<IInfo>();
            Func<S, string> getName;
            Func<S, string> getFolderName;
            
            public Action<GenericMenu, S[]> onContextMenu = delegate {};

            public ItemsList(List<S> collection, Func<S, string> getFolderName, Func<S, string> getName) : base(collection, null, new TreeViewState()) {
                this.getName = getName;
                this.getFolderName = getFolderName;
                onSelectionChanged += selection => {
                    selected.Clear();
                    selected.AddRange(selection);
                };
                Reload();
                ExpandAll();
            }

            protected override void RowGUI(RowGUIArgs args) {
                if (getFolderName == null) {
                    if (!info.ContainsKey(args.item.id)) return;
                    IInfo i = info[args.item.id];
                    if (i.isItemKind) DrawItem(args.rowRect, i.asItemKind);
                } else
                    base.RowGUI(args);
            }
            
            public override string GetPath(S element) {
                return getFolderName?.Invoke(element) ?? "";
            }

            public override void SetPath(S element, string path) {}
            
            public override int GetUniqueID(S element) {
                return element.GetHashCode();
            }

            public override S CreateItem() {
                return null;
            }
            
            public Type[] types;
            public Action<Type> newItem;
            public Action<S> duplicateItem;
            public Func<Rect, S, Rect> drawItem;

            public override void DrawItem(Rect rect, ItemInfo info) {
                if (drawItem != null)
                    rect = drawItem.Invoke(rect, info.content);

                if (rect.width > 0)
                    base.DrawItem(rect, info);
            }
            
            public override string GetLabel(ItemInfo info) {
                return getName == null ? info.content.ToString() : getName(info.content);
            }

            public override string GetName(S element) {
                return element.ToString();
            }

            
            public override void ContextMenu(GenericMenu menu, List<IInfo> selected) { 
                selected = selected.Where(x => x.isItemKind).ToList();
                if (selected.Count == 0) {
                    if (newItem != null)
                        foreach (Type type in types) {
                            var t = type;
                            menu.AddItem(new GUIContent($"Add/{type.FullName.Replace(".", "/")}"), 
                                false, () => newItem(t));
                        }
                } else {
                    if (selected.Count == 1) {
                        if (duplicateItem != null) 
                            menu.AddItem(new GUIContent("Duplicate"), false, () => duplicateItem(selected[0].asItemKind.content));
                        if (selected[0].asItemKind.content is ISerializableID sid) 
                            menu.AddItem(new GUIContent("Copy ID"), false, () => EditorGUIUtility.systemCopyBuffer = sid.ID);
                        menu.AddItem(new GUIContent("Edit"), false, () => ObjectEditorWindow
                            .Show(selected[0].asItemKind.content, this, null, null, true));
                    }
                    
                    menu.AddItem(new GUIContent("Remove"), false, () => Remove(selected.ToArray()));
                } 
                
                onContextMenu.Invoke(menu, selected.Select(x => x.asItemKind.content).ToArray());
            }

            protected override bool CanRename(TreeViewItem item) {
                return false;
            }
        }
    }
    
    class NewItemPopup : DashboardPopup {
        Func<bool> onNewItemGui;
        GUIHelper.Scroll scroll = new GUIHelper.Scroll();

        public void Setup(string title, Func<bool> onNewItemGui) {
            this.title = title;
            this.onNewItemGui = onNewItemGui;
            level = Level.Editor;
        }

        public override Vector2 GetSize() {
            return new Vector2(400, 400);
        }

        public override void OnGUI() {
            using (scroll.Start())
                if (!onNewItemGui.Invoke())
                    ClosePopup();
        }
    }

    public class StorageSelector<S> where S : class, ISerializable {
        Storage<S> storage;
        S selected = null;

        Func<S, string> getName;
        Func<S, bool> filter;
        
        public int Count => filter == null ? storage.items.Count : storage.items.Count(filter);
        
        public StorageSelector(Storage<S> storage, Func<S, string> getName, Func<S, bool> filter = null) {
            this.storage = storage;
            this.getName = getName;
            SetFilter(filter);
        }

        public void SetFilter(Func<S, bool> filter = null) {
            this.filter = filter;
            Select();
        }
        
        public void Draw(string label, Action<S> onChange, params GUILayoutOption[] options) {
            GUIHelper.Popup(label, selected, storage.items, EditorStyles.popup, getName, s => {
                selected = s;
                onChange?.Invoke(s);
            }, filter, options);
        }

        IEnumerable<S> Values() {
            if (filter == null)
                return storage.items;
            else
                return storage.items.Where(filter);
        }

        public S Select(Func<S, bool> extraFilter = null) {
            if (extraFilter != null)
                selected = Values().FirstOrDefault(extraFilter.Invoke);
            
            if (selected == null && (filter == null || filter.Invoke(selected)))
                return selected;
            
            if (selected == null)
                selected = Values().IDefaultOrFirst();

            return selected;
        }
    }
}