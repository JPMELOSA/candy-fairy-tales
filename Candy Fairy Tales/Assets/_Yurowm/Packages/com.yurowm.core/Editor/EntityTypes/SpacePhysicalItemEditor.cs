﻿using Yurowm.ObjectEditors;

namespace Yurowm.Spaces {
    public class SpacePhysicalItemEditor : ObjectEditor<SpacePhysicalItem> {
        public override void OnGUI(SpacePhysicalItem item, object context = null) {
            BaseTypesEditor.SelectBody(item);
        }
    }
}