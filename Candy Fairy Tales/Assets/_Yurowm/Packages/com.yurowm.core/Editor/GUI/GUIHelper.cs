﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using Yurowm.Extensions;
using Yurowm.GUIStyles;
using Yurowm.Utilities;
using UColor = UnityEngine.Color;

namespace Yurowm.GUIHelpers {
    public static class GUIHelper {
        public class Vertical : IDisposable {
            static Vertical Instance = new Vertical();

            public static Vertical Start(params GUILayoutOption[] options) {
                GUILayout.BeginVertical(options);
                return Instance;
            }
            
            public static Vertical Start(UColor color) {
                var rect = EditorGUILayout.BeginVertical();
                if (Event.current.type == EventType.Repaint)
                    Handles.DrawSolidRectangleWithOutline(rect, color, UColor.clear);

                return Instance;
            }

            public static Vertical Start(GUIStyle style, params GUILayoutOption[] options) {
                GUILayout.BeginVertical(style, options);
                return Instance;
            }

            Vertical() { }

            public void Dispose() {
                GUILayout.EndVertical();
            }
        }

        public class Horizontal : IDisposable {
            static Horizontal Instance = new Horizontal();

            public static Horizontal Start(params GUILayoutOption[] options) {
                GUILayout.BeginHorizontal(options);
                return Instance;
            }
            
            public static Horizontal Start(UColor color) {
                var rect = EditorGUILayout.BeginHorizontal();
                if (Event.current.type == EventType.Repaint)
                    Handles.DrawSolidRectangleWithOutline(rect, color, UColor.clear);

                return Instance;
            }

            public static Horizontal Start(GUIStyle style, params GUILayoutOption[] options) {
                GUILayout.BeginHorizontal(style, options);
                return Instance;
            }

            Horizontal() { }

            public void Dispose() {
                GUILayout.EndHorizontal();
            }
        }
        
        public class Toolbar : IDisposable {
            static Toolbar Instance = new Toolbar();

            public static Toolbar Start() {
                GUILayout.BeginHorizontal(EditorStyles.toolbar, GUILayout.ExpandWidth(true));
                return Instance;
            }

            Toolbar() { }

            public void Dispose() {
                GUILayout.EndHorizontal();
            }
        }

        public class Spoiler : IDisposable {
            AnimBool animBool;
            EditorGUILayout.FadeGroupScope group;
            
            public Spoiler(bool shown = false) {
                animBool = new AnimBool(shown);
                animBool.valueChanged.AddListener(() => EditorWindow.focusedWindow.Repaint());
            }

            public Spoiler Start(string title) {
                animBool.target = EditorGUILayout.Foldout(animBool.target, title);
                group = new EditorGUILayout.FadeGroupScope(animBool.faded);
                EditorGUI.indentLevel ++;
                return this;
            }

            public bool IsVisible() {
                return group.visible;
            }

            public void Dispose() {
                group.Dispose();
                EditorGUI.indentLevel --;
            }
        }
        
        public class Scroll {
            Vector2 position = new Vector2();
            GUILayoutOption[] options = null;
            GUIStyle style = null;

            public Scroll(GUIStyle style, params GUILayoutOption[] options) : this(options) {
                this.style = style;
            }

            public Scroll(params GUILayoutOption[] options) {
                this.options = options;
            }

            public ScrollDisposable Start() {
                return new ScrollDisposable(ref position, style, options);
            }

            public void ScrollTo(float y = 0) {
                position.y = y;
            }

            public class ScrollDisposable : IDisposable {

                public ScrollDisposable(ref Vector2 position, GUIStyle style, params GUILayoutOption[] options) {
                    if (style == null)
                        position = GUILayout.BeginScrollView(position, options);
                    else
                        position = GUILayout.BeginScrollView(position, style, options);
                }

                public void Dispose() {
                    GUILayout.EndScrollView();
                }
            }
        }

        public class Lock : IDisposable {
            static readonly Lock Instance = new Lock();
            
            readonly List<bool> memory = new List<bool>();
            
            Lock() {}

            public static Lock Start(bool value) {
                Instance.memory.Add(GUI.enabled);
                if (GUI.enabled)
                    GUI.enabled = !value;
                return Instance;
            }

            public void Dispose() {
                GUI.enabled = memory.All(v => v);
                memory.RemoveAt(memory.Count - 1);
            }
        }
        public class Clip : IDisposable {
            static readonly Clip Instance = new Clip();

            Clip() {}
            
            public static Clip Start(Rect rect) {
                GUI.BeginClip(rect);
                return Instance;
            }
                
            public static Clip Start(Rect rect, out Rect clipedRect) {
                clipedRect = rect;
                clipedRect.position = Vector2.zero;
                return Start(rect);
            }

            public void Dispose() {
                GUI.EndClip();
            }
        }

        public class Color : IDisposable {
            static readonly Color Instance = new Color();

            readonly List<UColor> memory = new List<UColor>();
            
            Color() {}

            public static Color Start(UColor color) {
                Instance.memory.Add(GUI.color);
                GUI.color = color;
                return Instance;
            }
                
            public static Color ProLiteStart() {
                return Start(EditorGUIUtility.isProSkin ? UColor.white : UColor.black);
            }

            public void Dispose() {
                GUI.color = memory[memory.Count - 1];
                memory.RemoveAt(memory.Count - 1);
            }

        }

        public class ContentColor : IDisposable {
            static readonly ContentColor Instance = new ContentColor();

            readonly List<UColor> memory = new List<UColor>();
            
            ContentColor() {}

            public static ContentColor Start(UColor color) {
                Instance.memory.Add(GUI.color);
                GUI.contentColor = color;
                return Instance;
            }

            public void Dispose() {
                GUI.contentColor = memory[memory.Count - 1];
                memory.RemoveAt(memory.Count - 1);
            }
        }

        public class BackgroundColor : IDisposable {
            static readonly BackgroundColor Instance = new BackgroundColor();

            readonly List<UColor> memory = new List<UColor>();
            
            BackgroundColor() {}

            public static BackgroundColor Start(UColor color) {
                Instance.memory.Add(GUI.color);
                GUI.backgroundColor = color;
                return Instance;
            }

            public void Dispose() {
                GUI.backgroundColor = memory[memory.Count - 1];
                memory.RemoveAt(memory.Count - 1);
            }
        }

        public class HandlesColor : IDisposable {
            static readonly HandlesColor Instance = new HandlesColor();

            readonly List<UColor> memory = new List<UColor>();
            
            HandlesColor() {}

            public static HandlesColor Start(UColor color) {
                Instance.memory.Add(Handles.color);
                Handles.color = color;
                return Instance;
            }

            public void Dispose() {
                Handles.color = memory[memory.Count - 1];
                memory.RemoveAt(memory.Count - 1);
            }
        }

        public class Change : IDisposable {
            static Change instance = new Change();
            
            List<Action> actionsQueue = new List<Action>();
            
            Change() {}
            
            public static Change Start(Action onChange) {
                EditorGUI.BeginChangeCheck();
                instance.actionsQueue.Add(onChange);
                return instance;
            }

            public void Dispose() {
                if (EditorGUI.EndChangeCheck())
                    actionsQueue[actionsQueue.Count - 1].Invoke();
                actionsQueue.RemoveAt(actionsQueue.Count - 1);
            }
        }     
        
        public class IndentLevel : IDisposable {
            static IndentLevel instance = new IndentLevel();
            
            IndentLevel() {}
            
            public static IndentLevel Start() {
                EditorGUI.indentLevel++;
                return instance;
            }

            public void Dispose() {
                EditorGUI.indentLevel--;
            }
        }
        
        public class Area : IDisposable {
            static Area instance = new Area();
            
            Area() {}
            
            public static Area Start(string title) {
                EditorGUILayout.LabelField(title, Styles.microTitle);
                EditorGUI.indentLevel++;
                return instance;
            }
            
            public static Area Start(string title, Action menuAction) {
                using (Horizontal.Start()) {
                    EditorGUILayout.PrefixLabel(title);
                    if (GUILayout.Button("...")) menuAction.Invoke();
                }
                EditorGUI.indentLevel++;
                return instance;
            }

            public void Dispose() {
                EditorGUI.indentLevel--;
            }
        }
        
        public class EditorLabelWidth : IDisposable {
            static readonly EditorLabelWidth Instance = new EditorLabelWidth();

            readonly List<float> memory = new List<float>();
            
            EditorLabelWidth() {}

            public static EditorLabelWidth Start(float value) {
                Instance.memory.Add(EditorGUIUtility.labelWidth);
                EditorGUIUtility.labelWidth = value;
                return Instance;
            }

            public void Dispose() {
                EditorGUIUtility.labelWidth = memory[memory.Count - 1];
                memory.RemoveAt(memory.Count - 1);
            }
        }

        public class LayoutSplitter {
            int resize = -1;
            int current = 0;
            float[] sizes;

            Rect lastRect;

            public float thickness = 2;

            OrientationLine orientation;
            OrientationLine internalOrientation;

            bool areaStarted = false;
            bool firstArea = false;

            public LayoutSplitter(OrientationLine orientation, OrientationLine internalOrientation, params float[] sizes) {
                this.sizes = sizes;
                this.orientation = orientation;
                this.internalOrientation = internalOrientation;
            }

            UColor separatorColor = new UColor(0, 0, 0, .1f); 
            UColor separatorProColor = new UColor(1, 1, 1, .1f); 
            
            public bool Area(GUIStyle style = null) {
                if (IsLast() && !firstArea)
                    return false;

                if (mask != null && !mask[current]) {
                    current++;
                    return false;
                }

                if (areaStarted) {
                    areaStarted = false;
                    EndLayout(internalOrientation);
                }
                if (!firstArea) {
                    lastRect = GUILayoutUtility.GetLastRect();
                    Rect rect = EditorGUILayout.GetControlRect(Expand(Anti(orientation)), Size(orientation, thickness));
                    
                    Handles.DrawSolidRectangleWithOutline(rect, 
                        EditorGUIUtility.isProSkin ? separatorProColor : separatorColor, 
                        UColor.clear);

                    EditorGUIUtility.AddCursorRect(rect, orientation == OrientationLine.Horizontal ? MouseCursor.ResizeHorizontal : MouseCursor.ResizeVertical);

                    if (resize < 0 && Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
                        resize = current;

                    if (resize == current) {
                        float delta = 0;
                        switch (orientation) {
                            case OrientationLine.Horizontal: delta = Mathf.Max(lastRect.width - (rect.x - Event.current.mousePosition.x + thickness / 2), 50); break;
                            case OrientationLine.Vertical: delta = Mathf.Max(lastRect.height - (rect.y - Event.current.mousePosition.y + thickness / 2), 50); break;
                        }

                        delta -= sizes[current];

                        sizes[current] += delta;

                        if (EditorWindow.mouseOverWindow)
                            EditorWindow.mouseOverWindow.Repaint();

                        if (Event.current.type == EventType.MouseUp)
                            resize = -1;
                    }
                }

                if (firstArea)
                    firstArea = false;
                else
                    current++;

                if (IsLast())
                    BeginLayout(internalOrientation, style);
                else
                    BeginLayout(internalOrientation, style, Size(orientation, sizes[current]));

                areaStarted = true;

                if (mask != null)
                    return mask[current];

                return true;
            }

            OrientationLine Anti(OrientationLine o) {
                switch (o) {
                    case OrientationLine.Horizontal: return OrientationLine.Vertical;
                    case OrientationLine.Vertical: return OrientationLine.Horizontal;
                }
                return OrientationLine.Horizontal;
            }

            Rect BeginLayout(OrientationLine o, GUIStyle style = null, params GUILayoutOption[] options) {
                switch (o) {
                    case OrientationLine.Horizontal: return style == null ? EditorGUILayout.BeginHorizontal(options) : EditorGUILayout.BeginHorizontal(style, options);
                    case OrientationLine.Vertical: return style == null ? EditorGUILayout.BeginVertical(options) : EditorGUILayout.BeginVertical(style, options);
                }
                return new Rect();
            }

            void EndLayout(OrientationLine o, params GUILayoutOption[] options) {
                switch (o) {
                    case OrientationLine.Horizontal: GUILayout.EndHorizontal(); break;
                    case OrientationLine.Vertical: GUILayout.EndVertical(); break;
                }
            }

            GUILayoutOption Size(OrientationLine o, float size) {
                switch (o) {
                    case OrientationLine.Horizontal: return GUILayout.Width(size);
                    case OrientationLine.Vertical: return GUILayout.Height(size);
                }
                return null;
            }

            GUILayoutOption Expand(OrientationLine o) {
                switch (o) {
                    case OrientationLine.Horizontal: return GUILayout.ExpandWidth(true);
                    case OrientationLine.Vertical: return GUILayout.ExpandHeight(true);
                }
                return null;
            }

            bool[] mask = null;
            public Splitter Start(params bool[] mask) {
                last = sizes.Length;
                UpdateMask(mask);

                current = 0;
                areaStarted = false;
                firstArea = true;

                BeginLayout(orientation, null, Expand(orientation));

                if (current >= sizes.Length)
                    return new Splitter(() => EndLayout(orientation));

                return new Splitter(() => {
                    if (areaStarted)
                        EndLayout(internalOrientation);
                    EndLayout(orientation);
                });
            }

            public void UpdateMask(params bool[] mask) {
                if (mask != null && mask.Length == sizes.Length + 1) {
                    this.mask = mask;
                    while (last >= 0 && !mask[last])
                        last--;
                } else
                    this.mask = null;
            }

            int last = -1;
            bool IsLast() {
                return current >= last;
            }

            public class Splitter : IDisposable {
                Action onDispose;

                public Splitter(Action onDispose) {
                    this.onDispose = onDispose;
                }

                public void Dispose() {
                    onDispose();
                }
            }
        }

        public static void EditRange(string label, IntRange range, int minLimit, int maxLimit) {
            if (range == null) return;

            float min = range.Min;
            float max = range.Max;

            using (Horizontal.Start()) {
                EditorGUILayout.MinMaxSlider(label, ref min, ref max, minLimit, maxLimit);
                range.min = EditorGUILayout.IntField(Mathf.RoundToInt(min), GUILayout.Width(40));
                range.max = EditorGUILayout.IntField(Mathf.RoundToInt(max), GUILayout.Width(40));
            }
        }
        
        public static T Popup<T>(string label, T selected, IEnumerable<T> collection, GUIStyle style, 
            Func<T, string> getName, Action<T> onChange, Func<T, bool> filter = null,
            params GUILayoutOption[] options) where T : class {
            
            Rect rect = EditorGUILayout.GetControlRect(true, EditorGUIUtility.singleLineHeight, style, options);
            if (!label.IsNullOrEmpty())
                rect = EditorGUI.PrefixLabel(rect, new GUIContent(label));
            
            
            if (collection == null) {
                GUI.Label(rect, "<NO OPTIONS>", style);
                return selected;
            }

            if (filter != null && !filter.Invoke(selected)) 
                selected = collection.FirstOrDefault(filter);
            
            if (GUI.Button(rect, GetName(selected, getName), style)) {
                GenericMenu menu = new GenericMenu();

                if (filter == null || filter.Invoke(null)) {
                    menu.AddItem(new GUIContent(GetName(null, getName)), selected == null, () => {
                        selected = null;
                        onChange?.Invoke(selected);
                        GUI.changed = true;
                    });
                }
                    
                foreach (T item in collection) {
                    if (filter != null && !filter.Invoke(item)) continue;
                    T _item = item;
                    menu.AddItem(new GUIContent(GetName(item, getName)), item.Equals(selected), () => {
                        if (selected != null && selected.Equals(_item)) return;
                        selected = _item;
                        onChange?.Invoke(selected);
                        GUI.changed = true;
                    });
                }
                
                menu.DropDown(rect);
            }
            
            return selected;
        }
        
        static string GetName<T>(T item, Func<T, string> getName) where T : class {
            if (item == null) return "<NULL>";
            return getName?.Invoke(item) ?? "<NO NAME>";
        }
        
        public static void DrawSprite(Sprite sprite) {
            DrawSprite(sprite, sprite.texture.width, sprite.texture.height);
        }

        public static void DrawSprite(Sprite sprite, float width, float height) {
            Rect rect = EditorGUILayout.GetControlRect(GUILayout.Width(width), GUILayout.Height(height));
            if (sprite != null) {
                Rect uv = sprite.rect;
                uv.x /= sprite.texture.width;
                uv.width /= sprite.texture.width;
                uv.y /= sprite.texture.height;
                uv.height /= sprite.texture.height;
                GUI.DrawTextureWithTexCoords(rect, sprite.texture, uv);
            }
        }


        static Texture2D _wireTexture = null;
        static Texture2D wireTexture {
            get {
                if (_wireTexture == null) {
                    _wireTexture = new Texture2D(1, 5, TextureFormat.RGBA4444, false);

                    for (int x = 0; x < _wireTexture.width; x++)
                        for (int y = 0; y < _wireTexture.height; y++)
                            _wireTexture.SetPixel(x, y, y == 0 || y == _wireTexture.height - 1 ?
                                UnityEngine.Color.clear : UnityEngine.Color.white);

                    _wireTexture.Apply();
                }
                return _wireTexture;
            }

        }

        public static void DrawBezier(Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent, UnityEngine.Color startColor, UnityEngine.Color endColor, float width) {
            if (Event.current.type != EventType.Repaint) return;

            Vector3 last = startPosition;
            Vector3 current;
            Vector3 q0, q1, q2, r0, r1;
            float delta = 1f / 40;
            UColor color = Handles.color;
            for (float t = 0; last != endPosition; t += delta) {
                q0 = Vector3.Lerp(startPosition, startTangent, t);
                q1 = Vector3.Lerp(startTangent, endTangent, t);
                q2 = Vector3.Lerp(endTangent, endPosition, t);
                r0 = Vector3.Lerp(q0, q1, t);
                r1 = Vector3.Lerp(q1, q2, t);

                current = Vector3.Lerp(r0, r1, t);
                Handles.color = UnityEngine.Color.Lerp(startColor, endColor, t) * color;
                Handles.DrawAAPolyLine(wireTexture, width, last, current);
                last = current;
            }
            Handles.color = color;
        }
        
        public static void DrawLine(Vector3 startPosition, Vector3 endPosition, UnityEngine.Color color, float width) {
            if (Event.current.type != EventType.Repaint) return;
            
            using (HandlesColor.Start(color))
                Handles.DrawAAPolyLine(wireTexture, width, startPosition, endPosition);
        }
        
        public static void DrawRectLine(Rect rect, UnityEngine.Color color, float width) {
            if (Event.current.type != EventType.Repaint) return;
            
            using (HandlesColor.Start(color)) {
                Handles.DrawAAPolyLine(wireTexture, width, new Vector3(rect.xMin, rect.yMin), new Vector3(rect.xMax, rect.yMin));
                Handles.DrawAAPolyLine(wireTexture, width, new Vector3(rect.xMax, rect.yMin), new Vector3(rect.xMax, rect.yMax));
                Handles.DrawAAPolyLine(wireTexture, width, new Vector3(rect.xMax, rect.yMax), new Vector3(rect.xMin, rect.yMax));
                Handles.DrawAAPolyLine(wireTexture, width, new Vector3(rect.xMin, rect.yMax), new Vector3(rect.xMin, rect.yMin));
            }
        }

        public class SearchPanel {
            static GUIStyle _searchStyle, _searchXStyle, _keyItemStyle;
            static GUIStyle searchStyle {
                get {
                    if (_searchStyle == null)
                        _searchStyle = GUI.skin.FindStyle("ToolbarSeachTextField");
                    return _searchStyle;
                }
            }
            static GUIStyle searchXStyle {
                get {
                    if (_searchXStyle == null)
                        _searchXStyle = GUI.skin.FindStyle("ToolbarSeachCancelButton");
                    return _searchXStyle;
                }
            }
            public static GUIStyle keyItemStyle {
                get {
                    if (_keyItemStyle == null) {
                        _keyItemStyle = new GUIStyle(EditorStyles.label);
                        _keyItemStyle.richText = true;      
                        _keyItemStyle.wordWrap = false;
                    }
                    return _keyItemStyle;
                }
            }

            string search;
            public string value {
                get {
                    return search;
                }
                set {
                    search = value;
                }
            }

            public SearchPanel(string search) {
                this.search = search;
            }

            public void OnGUI(Action<string> onChanged, params GUILayoutOption[] options) {
                using (Change.Start(() => onChanged(search))) {
                    search = GUILayout.TextField(search, searchStyle, options);
                    if (GUILayout.Button("", searchXStyle)) {
                        search = "";
                        EditorGUI.FocusTextInControl("");
                    }
                }
            }

            public static string Format(string text, string search) {
                int index = text.ToLower().IndexOf(search.ToLower());
                if (index < 0) return text;
                string result = text.Substring(0, index);
                result += string.Format(Styles.highlightStrongRed, text.Substring(index, search.Length));
                result += text.Substring(index + search.Length);
                return result;
            }
        }

        public static bool Button(string label, string buttonLabel, GUIStyle style = null) {
            using (Horizontal.Start()) {
                EditorGUILayout.PrefixLabel(label);
                return style == null ? GUILayout.Button(buttonLabel) : GUILayout.Button(buttonLabel, style);
            }
        }
    }
}