using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Yurowm.ContentManager;
using Yurowm.Extensions;
using Yurowm.Serialization;
using Yurowm.Spaces;
using Yurowm.Utilities;

namespace YMatchThree.Core {
    public class Walls : LevelSlotExtension {
        
        WallsVariable info;
        
        public string borderRenderer;
        BorderRenderer renderer;
        
        public override void OnAddToSpace(Space space) {
            base.OnAddToSpace(space);
            
            var slots = context.GetArgument<Slots>();
            
            List<int2> vertWall = new List<int2>();
            List<int2> horizWall = new List<int2>();
            
            foreach (Slot slot in slots.all.Values) {
                foreach (var side in Sides.straight) {
                    if (!slots.all.ContainsKey(slot.coordinate + side)) continue;
                    if (Wall.IsWall(slot.coordinate, side, slots.all.Keys, info)) {
                        slot.nearSlot[side] = null;
                        if (side == Side.Top) horizWall.Add(slot.coordinate + int2.up);
                        else if (side == Side.Left) vertWall.Add(slot.coordinate);
                    }
                }
            }
            
            foreach (Slot slot in slots.all.Values) {
                foreach (var side in Sides.slanted) {
                    if (!slots.all.ContainsKey(slot.coordinate + side)) continue;
                    if (!slot[side]) continue;
                    
                    if (!slot[side.Rotate(1)] && !slot[side.Rotate(-1)]) {
                        slot[side] = null;
                        continue;
                    }
                    
                    if (!slot[side.Rotate(1)]?[side.Rotate(-2)] || !slot[side.Rotate(-1)]?[side.Rotate(2)])
                        slot[side] = null;
                }
            }
            
            slots.all.Values.ForEach(x => x.CalculateFallingSlot());
            
            if (!borderRenderer.IsNullOrEmpty()) {
                renderer = AssetManager.Emit<BorderRenderer>(borderRenderer, context);
                renderer.transform.SetParent(space.root);
                renderer.transform.Reset();
                renderer.Rebuild(
                    info.orientations
                        .Where(o => o.Value.HasFlag(OrientationLine.Horizontal))
                        .Select(o => o.Key)
                        .ToList(),
                    info.orientations
                        .Where(o => o.Value.HasFlag(OrientationLine.Vertical))
                        .Select(o => o.Key)
                        .ToList());
            }
        }
        
        public override Type GetContentBaseType() {
            return typeof (LevelExtension);
        }
        
        public override void SetupVariable(ISerializable variable) {
            base.SetupVariable(variable);
            switch (variable) {
                case WallsVariable walls: info = walls; return;
            }
        }

        public override IEnumerator GetVariblesTypes() {
            yield return base.GetVariblesTypes();
            yield return typeof(WallsVariable);
        }
        
        
        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("borderRenderer", borderRenderer);
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("borderRenderer", out borderRenderer);
        }

        #endregion
        
        public struct Wall {
            public bool exist;
            public int2 coord;
            public bool horiz;
            
            public static bool IsWall(int2 coord, Side side, ICollection<int2> slots, WallsVariable info) {
                Wall wall = GetWall(coord, side, slots);
            
                if (!wall.exist) return false;

                if (info.orientations.TryGetValue(wall.coord, out var orientation))
                    return orientation.HasFlag(wall.horiz ? OrientationLine.Horizontal : OrientationLine.Vertical);

                return false;
            }
        
            public static Wall GetWall(int2 coord, Side side, ICollection<int2> slots) {
                Wall result = new Wall();
            
                result.exist = true;

                if (side.IsSlanted() || !slots.Contains(coord + side)) {
                    result.exist = false;
                    return result;
                }

                result.coord = coord;
                result.horiz = side.Y() == 0;
                if ((result.horiz ? side.X() : side.Y()) > 0) result.coord += side;

                return result;
            }
        }
    }
    
    public class WallsVariable : ContentInfoVariable {
        public Dictionary<int2, OrientationLine> orientations = new Dictionary<int2, OrientationLine>();
        
        public override void Serialize(Writer writer) {
            writer.Write("orientationKeys", orientations.Keys.ToArray());
            writer.Write("orientationValues", orientations.Values.Cast<int>().ToArray());
        }

        public override void Deserialize(Reader reader) {
            var keys = reader.ReadCollection<int2>("orientationKeys");
            var values = reader.ReadCollection<int>("orientationValues");
            
            orientations.Clear();
            orientations.AddPairs(keys.Zip(values, (c, o) 
                => new KeyValuePair<int2, OrientationLine>(c, (OrientationLine) o)));
        }
    }
}