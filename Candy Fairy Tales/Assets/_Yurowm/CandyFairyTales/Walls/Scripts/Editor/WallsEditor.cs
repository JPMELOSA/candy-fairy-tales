using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YMatchThree.Core;
using Yurowm.ContentManager;
using Yurowm.ObjectEditors;
using Yurowm.Spaces;

namespace YMatchThree.Editor {
    public class WallsEditor : ObjectEditor<Walls> {
        public override void OnGUI(Walls walls, object context = null) {
            BaseTypesEditor.SelectAsset<BorderRenderer>(walls, nameof(walls.borderRenderer));
        }
    }
}