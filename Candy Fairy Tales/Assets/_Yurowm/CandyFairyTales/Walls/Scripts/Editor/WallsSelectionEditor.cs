using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using YMatchThree.Editor;
using Yurowm;
using Yurowm.Extensions;
using Yurowm.GUIHelpers;
using Yurowm.GUIStyles;
using Yurowm.Utilities;

namespace YMatchThree.Core {
    public class WallsSelectionEditor : LevelSlotExtensionSelectionEditor<Walls> {
        
        public override void OnGUI(SlotInfo[] slots, ContentInfo extension, LevelFieldEditor fieldEditor) {
            DrawOutlineGUI(slots, extension, fieldEditor);
            DrawSidesGUI(slots, extension, fieldEditor);
        }

        #region Outline

        void DrawOutlineGUI(SlotInfo[] slots, ContentInfo extension, LevelFieldEditor fieldEditor) {
            Rect rect = EditorGUILayout.GetControlRect(GUILayout.ExpandWidth(true), GUILayout.Height(EditorGUIUtility.singleLineHeight));

            if (Event.current.type == EventType.Layout) return;

            Rect rect2 = EditorGUI.PrefixLabel(rect, new GUIContent("Outline"));
            rect.xMin = rect2.x;

            int exist = 0;
            int notexist = 0;
            
            var info = extension.GetVariable<WallsVariable>();
            
            foreach (var slot in slots) {
                foreach (var side in Sides.straight)
                    if (fieldEditor.slots.ContainsKey(slot.coordinate + side) 
                        && slots.All(s => s.coordinate != slot.coordinate + side)) {
                        
                        if (Walls.Wall.IsWall(slot.coordinate, side, fieldEditor.slots.Keys, info)) 
                            exist++; 
                        else
                            notexist++;
                    }
            }

            if (exist > 0 && notexist > 0) {
                if (GUI.Button(rect, "[Set]", EditorStyles.miniButton)) SetOutline(true, slots, fieldEditor, info);
            } else if (exist > 0) {
                if (GUI.Button(rect, "Remove", EditorStyles.miniButton)) SetOutline(false, slots, fieldEditor, info);
            } else if (notexist > 0) {
                if (GUI.Button(rect, "Set", EditorStyles.miniButton)) SetOutline(true, slots, fieldEditor, info);
            } else {
                GUI.Button(rect, "-", EditorStyles.miniButton);
            }
        }

        void SetOutline(bool value, SlotInfo[] slots, LevelFieldEditor fieldEditor, WallsVariable info) {
            foreach (var slot in slots) {
                if (!fieldEditor.slots.ContainsKey(slot.coordinate)) continue;
                foreach (Side side in Sides.straight)
                    if (slots.All(s => s.coordinate != slot.coordinate + side))
                        SetWall(value, slot.coordinate, side, fieldEditor.slots.Keys, info);
            }
        }

        #endregion

        #region Sides
        
        Dictionary<Side, Rect> sideButtons = new Dictionary<Side, Rect>();
        
        static readonly Color pressedColor = new Color(0.6f, 0.39f, 0.3f);
        static readonly Color unpressedColor = new Color(0.65f, 0.65f, 0.65f);
        static readonly Color unknownColor = Color.Lerp(pressedColor, unpressedColor, .5f);
        
        void DrawSidesGUI(SlotInfo[] slots, ContentInfo extension, LevelFieldEditor fieldEditor) {
            Rect rect = EditorGUILayout.GetControlRect(GUILayout.ExpandWidth(true), GUILayout.Height(80));   
            
            if (Event.current.type == EventType.Layout) return;
            
            var info = extension.GetVariable<WallsVariable>();
            
            Rect rect2 = EditorGUI.PrefixLabel(rect, new GUIContent("Sides"));
            rect.xMin = rect2.x;

            rect2.width = Mathf.Min(rect2.width, rect.height);

            float buttonSize = EditorGUIUtility.singleLineHeight;
            sideButtons[Side.Left] = new Rect(rect2.xMin, rect2.yMin + buttonSize, buttonSize, rect2.height - buttonSize * 2);
            sideButtons[Side.Right] = new Rect(rect2.xMax - buttonSize, rect2.yMin + buttonSize, buttonSize, rect2.height - buttonSize * 2);
            sideButtons[Side.Bottom] = new Rect(rect2.xMin + buttonSize, rect2.yMax - buttonSize, rect2.width - buttonSize * 2, buttonSize);
            sideButtons[Side.Top] = new Rect(rect2.xMin + buttonSize, rect2.yMin, rect2.width - buttonSize * 2, buttonSize);
            
            
            
            foreach (var side in sideButtons) {
                EUtils.DrawMixedProperty(slots
                        .Where(s => fieldEditor.slots.ContainsKey(s.coordinate + side.Key)),
                    slot => Walls.Wall.IsWall(slot.coordinate, side.Key, fieldEditor.slots.Keys, info),
                    (slot, value) => {
                        if (value != Walls.Wall.IsWall(slot.coordinate, side.Key, fieldEditor.slots.Keys, info))
                            SetWall(value, slot.coordinate, side.Key, fieldEditor.slots.Keys, info);
                    },
                    (position, value) => {
                        using (GUIHelper.Color.Start(value ? pressedColor : unpressedColor))
                            return GUI.Toggle(side.Value, value, "", Styles.paperArea);
                    },
                    (value, callback) => {
                        using (GUIHelper.Color.Start(unknownColor))
                            if (GUI.Toggle(side.Value, value, "*", Styles.paperArea) != value)
                                callback(!value);
                    }
                );
            }
        }
        
        #endregion
        
        void SetWall(bool value, int2 coord, Side side, ICollection<int2> slots, WallsVariable info) {
            Walls.Wall wall = Walls.Wall.GetWall(coord, side, slots);

            if (!wall.exist) return;

            var orientation = info.orientations.Get(wall.coord);
            var orientationRef = wall.horiz ? OrientationLine.Horizontal : OrientationLine.Vertical;

            if (orientation.HasFlag(orientationRef) == value) return;

            if (value)
                orientation = orientation | orientationRef;
            else
                orientation = orientation & ~orientationRef;
            
            if (orientation == 0)
                info.orientations.Remove(wall.coord);
            else
                info.orientations[wall.coord] = orientation;
        }
    }
}