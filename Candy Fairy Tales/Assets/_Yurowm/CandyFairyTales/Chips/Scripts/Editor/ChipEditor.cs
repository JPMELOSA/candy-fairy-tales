using UnityEngine;
using YMatchThree.Core;
using Yurowm.GUIHelpers;
using Yurowm.Icons;

namespace YMatchThree.Editor {
    
    public class ChipSlotDrawer : SlotContentDrawer {
        
        Texture2D icon;
        
        public override int Order => 1;
        
        public override void DrawSlot(Rect rect, ContentInfo info, LevelFieldEditor editor) {
            if (info.Reference is Chip chip) {
                if (!icon)
                    icon = EditorIcons.GetIcon("ChipIcon");
        
                var colored = info.GetVariable<ColoredVariable>();
                if (colored != null && colored.info.type == ItemColor.Known) {
                    using (GUIHelper.Color.Start(Color.Lerp(ItemColorEditorPalette.Get(colored.info.ID), Color.white, 0.4f)))
                        GUI.DrawTexture(rect, icon);
                } else
                    GUI.DrawTexture(rect, icon);
                
                var layered = info.GetVariable<LayeredVariable>();
                
                GUI.Label(rect, chip.shortName + (layered != null ? $":{layered.count}" : ""), LevelEditorStyles.labelStyle);
            }
        }
    }
}