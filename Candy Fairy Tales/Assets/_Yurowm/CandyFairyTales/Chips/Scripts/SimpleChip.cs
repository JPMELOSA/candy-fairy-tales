using Yurowm.Colors;
using Yurowm.Extensions;
using Yurowm.Serialization;

namespace YMatchThree.Core {
    [SerializeShort]
    public class SimpleChip : WhiteChip, IColored, IDefaultSlotContent {
        public override SpaceObject EmitBody() {
            
            var generalChipBody = context.GetArgument<LevelScriptBase>().chipBody;
            
            if (!generalChipBody.IsNullOrEmpty())
                bodyName = generalChipBody;
            
            return base.EmitBody();
        }
    }
}