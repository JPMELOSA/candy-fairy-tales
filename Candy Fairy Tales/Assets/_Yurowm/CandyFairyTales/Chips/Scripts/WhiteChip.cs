using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YMatchThree.Core {
    public class WhiteChip : Chip, IDestroyable, IShuffled {       
        public int scoreReward { get; set; }
        
        public bool IsSuitableForNewSlot(Level level, SlotInfo slot) {
            return true;
        }

        public IEnumerator Destroying() { 
            yield return PlayClipAndWait("Destroying");
        }
    }
}