using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Yurowm.ContentManager;
using Yurowm.Coroutines;
using Yurowm.Effects;
using Yurowm.Extensions;
using Yurowm.Serialization;
using Yurowm.Utilities;

namespace YMatchThree.Core {
    public abstract class NodeBomb : BombChipBase {
        
        public string nodeEffect;
        
        public ExplosionParameters explosion = new ExplosionParameters();
        
        public override IEnumerator Exploding() {
            PlayClip("Destroying");
            
            yield return EmitNodes(OnReachedTheTarget);
            
            if (animator) 
                yield return animator.WaitPlaying();
        }

        protected IEnumerator EmitNodes(Action<Effect, Slot, HitContext> onReachTarget) {
            
            var hitGroup = slotModule.Slots().ToList();
            
            var targets = GetNodeTargets()
                .Where(t => t != this)
                .Select(t => t.slotModule.Slot())
                .ToList();
            
            hitGroup.AddRange(targets);
            
            var hitContext = new HitContext(context, hitGroup.Distinct(), HitReason.BombExplosion);
            
            field.Highlight(hitContext.group, slotModule.Slot());

            if (!nodeEffect.IsNullOrEmpty() && simulation.AllowEffects()) {
                int wait = 0;
                
                foreach (var target in targets) {
                    var _target = target;
                    
                    Effect effect = null;
                    
                    var callback = new NodeEffectLogicProvider.Callback();
                    
                    callback.targetProvider = new NodeEffectItemProvider(target);
                    
                    callback.interpolator = new NodeEffectCurveInterpolator(
                            YRandom.main.Range(.2f, .3f), 0, 0);

                    wait ++;
                    
                    callback.onComplete = () => wait --;
                    callback.onReachTarget = () => field.Explode(_target.position, explosion);
                    callback.onReachTarget += () => onReachTarget(effect, _target, hitContext);
                    
                    effect = Effect.Emit(field, nodeEffect, position, callback);
                    
                    field.AddContent(effect);
                    
                    OnCreateNodeEffect(effect);
                    
                    if (simulation.AllowToWait())
                        yield return null;
                }
                
                while (wait != 0)
                    yield return null;
                
            } else {
                targets.ForEach(t => field.Explode(t.position, explosion));
                targets.ForEach(t => onReachTarget(null, t, hitContext));
            }
        }
        
        public abstract IEnumerable<SlotContent> GetNodeTargets();
        
        public virtual void OnCreateNodeEffect(Effect effect) {}
        
        public abstract void OnReachedTheTarget(Effect effect, Slot target, HitContext hitContext);

        
        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("nodeEffect", nodeEffect);
            writer.Write("explosion", explosion);
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("nodeEffect", out nodeEffect);
            reader.Read("explosion", out explosion);
        }

        #endregion
    }
}