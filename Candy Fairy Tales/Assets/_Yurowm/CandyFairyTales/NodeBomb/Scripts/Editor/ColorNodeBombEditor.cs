using UnityEditor;
using YMatchThree.Core;
using Yurowm.ObjectEditors;

namespace YMatchThree.Editor {
    public class ColorNodeBombEditor : ObjectEditor<ColorNodeBomb> {
        public override void OnGUI(ColorNodeBomb bomb, object context = null) {
            bomb.MixPriority = EditorGUILayout.IntField("Mix Priority", bomb.MixPriority);
        }
    }
}