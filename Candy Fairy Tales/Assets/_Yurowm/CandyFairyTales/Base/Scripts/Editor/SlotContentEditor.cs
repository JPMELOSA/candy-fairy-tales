using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using YMatchThree.Core;
using Yurowm;
using Yurowm.ObjectEditors;
using Yurowm.Spaces;
using Yurowm.Utilities;

namespace YMatchThree.Editor {
    public class SlotContentEditor : ObjectEditor<SlotContent> {
        
        public class ChipEditor : ObjectEditor<Chip> {
            public override void OnGUI(Chip chip, object context = null) {
            }
        }
        
        public override void OnGUI(SlotContent content, object context = null) {
            content.shortName = EditorGUILayout.TextField("Short Name (2-3 chars)", content.shortName);
            content.color = EditorGUILayout.ColorField("Color", content.color); 
            BaseTypesEditor.SelectAsset<Sprite>(content, nameof(content.miniIcon));
            
            if (content is IDestroyable)
                content.destroyingDelay = EditorGUILayout.FloatField("Destroying Delay", content.destroyingDelay).ClampMin(0);
        }
    }
}