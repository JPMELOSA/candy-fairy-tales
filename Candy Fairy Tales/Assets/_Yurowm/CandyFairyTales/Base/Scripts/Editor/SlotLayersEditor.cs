using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using YMatchThree.Core;
using Yurowm.Extensions;
using Yurowm.HierarchyLists;

namespace YMatchThree.Editor {
    public class SlotLayersList : NonHierarchyList<SlotLayerBase> {
        LevelLayoutEditor editor;
        public readonly List<SlotLayerBase> selected = new List<SlotLayerBase>();

        public SlotLayersList(List<SlotLayerBase> collection, LevelLayoutEditor editor) : base(collection) {
            this.editor = editor;
            onChanged += editor.SetDirty;
            onSelectedItemChanged += list => {
                selected.Clear();
                selected.AddRange(list);
            };
        }

        public override int GetUniqueID(SlotLayerBase element) {
            return element.ID.GetHashCode();
        }

        public override void DrawItem(Rect rect, ItemInfo info) {
            if (selected.Contains(info.content))
                Handles.DrawSolidRectangleWithOutline(rect, Color.clear, Color.cyan);
            GUI.Label(rect, GetLabel(info), info.content.isDefault ? EditorStyles.boldLabel : EditorStyles.label);
        }

        public override SlotLayerBase CreateItem() {
            var result = newLayer;
            newLayer = null;
            return result;
        }

        public override string GetName(SlotLayerBase element) {
            return element.ID;
        }

        public override void SetName(SlotLayerBase element, string name) {
            element.ID = name;
        }

        SlotLayerBase newLayer;

        public override void ContextMenu(GenericMenu menu, List<IInfo> selected) {

            if (!editor.selected.IsEmpty())
                menu.AddItem(new GUIContent("New/Selection Layer"), false, () => {
                    var result = new SlotLayer();
                    result.SetSelection(editor.design.slots.Where(s => editor.selected.Contains(s.coordinate)));
                    newLayer = result;
                    if (itemCollection.All(l => !l.isDefault))
                        newLayer.isDefault = true;
                    AddNewItem(headFolder, null);
                });         
            
            if (itemCollection.All(l => !(l is AllSlotsLayer)))
                menu.AddItem(new GUIContent("New/All Slots Layer"), false, () => {
                    newLayer = new AllSlotsLayer();
                    if (itemCollection.All(l => !l.isDefault))
                        newLayer.isDefault = true;
                    AddNewItem(headFolder, null);
                });
            
            menu.AddItem(new GUIContent("Show All Slots"), false, func: () => {
                this.selected.Clear();
                this.selected.AddRange(editor.design.layers);
            });
            
            if (selected.Count == 1) {
                menu.AddItem(new GUIContent("Select"), false, () => {
                    var l = selected[0].asItemKind.content;
                    editor.selected = editor.design.slots
                        .Where(s => l.Contains(editor.design, s))
                        .Select(s => s.coordinate)
                        .ToList();
                });
                menu.AddItem(new GUIContent("Set Default"), false, func: () => {
                    var sl = selected[0].asItemKind.content;
                    itemCollection.ForEach(l => l.isDefault = l == sl);     
                    onChanged();
                });
            }
            
            if (selected.Count == 1 && selected[0].asItemKind.content is SlotLayer layer)
                menu.AddItem(new GUIContent("Set Selection"), false, () => {
                    layer.SetSelection(editor.design.slots.Where(s => editor.selected.Contains(s.coordinate)));
                    onChanged();
                });
                
            
            if (selected.Count > 0)
                menu.AddItem(new GUIContent("Remove"), false, () => Remove(selected.ToArray()));
        }
    }
}