using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using YMatchThree.Core;
using YMatchThree.Seasons;
using Yurowm;
using Yurowm.ContentManager;
using Yurowm.Coroutines;
using Yurowm.DebugTools;
using Yurowm.Effects;
using Yurowm.Extensions;
using Yurowm.Jobs;
using Yurowm.Localizations;
using Yurowm.Serialization;
using Yurowm.Utilities;
using Space = Yurowm.Spaces.Space;

namespace YMatchThree {
    public class Field : GameEntity, ISelfUpdate, ILiveContextHolder {
        public readonly LiveContext fieldContext;

        PuzzleSpace space;
        Level level;

        public LevelCamera camera;
        public LevelEvents events;
        
        public LevelGameplay gameplay;
        
        public Slots slots;
        
        public Transform root;
        public bool complete = false;
        
        public Field(Level level, PuzzleSpace space) {
            this.level = level;
            this.space = space;

            if (level.randomSeed == 0)
                level.randomSeed = YRandom.main.Seed();
            
            random = new YRandom(level.randomSeed);
            
            fieldContext = new LiveContext("LevelField");
            
            fieldContext.SetArgument(this);
            
            fieldContext.SetArgument(level);
            
            fieldContext.SetArgument(space.time);
            
            fieldContext.SetArgument(space);
            fieldContext.SetArgument<Space>(space);
            
            SetSimulation(new GeneralPuzzleSimulation());
        }

        public area Area => level.Area;
        public SlotLayerBase interactionLayer = null;

        public override void OnAddToSpace(Space space) {
            base.OnAddToSpace(space);
            root = new GameObject($"Field").transform;
            root.SetParent(space.root);
            root.Reset();
            space.BlindCatch<LevelCamera>(c => {
                camera = c;
            });
        }

        public readonly CoroutineCore coroutine = new CoroutineCore();
        
        public void UpdateFrame(Updater updater) {
            coroutine.Update(CoroutineCore.Loop.Update);
        }

        public void ForceUpdate() {
            space.time.Update();
            UpdateFrame(null);
        }
        
        public void Start() {
            fieldContext
                .Get<LevelGameplay>()
                .Start();
        }
        
        public IEnumerator BuildStage () {
            root.gameObject.SetActive(false);
            
            var access = new DelayedAccess(1f / 40);

            var script = context.GetArgument<LevelScriptBase>();
            fieldContext.SetArgument(script);
            
            var colorSettings = script.colorSettings.Clone();
            
            if (colorSettings.colorPalette == null)
                colorSettings.colorPalette = LevelContent.storage.
                    GetDefault<ItemColorPalette>()?.Clone();

            colorSettings.Initialize();
            
            fieldContext.SetArgument(colorSettings);
            AddContent(colorSettings.colorPalette);
            
            events = new LevelEvents();
            fieldContext.SetArgument(events);
            
            gameplay = LevelContent.GetItem<LevelGameplay>(level.gamePlay).Clone();
            AddContent(gameplay);
            
            slots = gameplay.slots;
            
            AddContent(LevelContent.GetItem<ChipPhysic>(level.physic).Clone());
            
            AddContent(new ChipGeneratorController());
            
            #region Creating new empty slot
            
            var defaultLayer = level.layers.FirstOrDefault(l => l.isDefault);
            
            foreach (var slotInfo in level.slots.Where(s => defaultLayer.Contains(level, s))) {
                CreateSlot(slotInfo);

                if (access.GetAccess()) yield return null;
            }
            
            #endregion
            
            foreach (var slotInfo in level.slots) {
                if (slots.all.TryGetValue(slotInfo.coordinate, out var slot)) {
                    FillSlot(slot, slotInfo);

                    if (access.GetAccess()) yield return null;
                }
            }
            
            slots.FirstBake();
            
            foreach (var extensionInfo in level.extensions) {
                var content = extensionInfo.Reference.Clone();

                content.ApplyDesign(extensionInfo);
                    
                AddContent(content);

                if (access.GetAccess()) yield return null;
            }
            
            camera.SetField(this);

            root.gameObject.SetActive(true);
        }

        public Slot CreateAndFillSlot(SlotInfo slotInfo) {
            var result = CreateSlot(slotInfo);
            FillSlot(result, slotInfo);
            return result;
        }
        
        public void FillSlot(Slot slot, SlotInfo slotInfo) {
            foreach (var contentInfo in slotInfo.Content()) {
                if (contentInfo.Reference is SlotContent) {
                    var content = contentInfo.Reference.Clone();
                    content.ApplyDesign(contentInfo);
                    content.position = slot.position;
                    AddContent(content);
                    slot.AddContent(content as SlotContent);
                }
            }
        }

        public Slot CreateSlot(SlotInfo slotInfo) {
            var result = new Slot();
            
            result.coordinate = slotInfo.coordinate;
            result.position = (result.coordinate.ToVector2() + Vector2.one / 2) 
                            * Slot.Offset;
            
            slots.all.Add(result.coordinate, result);
            
            AddContent(result);
            
            return result;
        }

        public void AddContent(LevelContent content) {
            if (fieldContext.Add(content)) {
                content.space = space;
                content.field = this;
                content.random = random.NewRandom();
                
                if (content is IPuzzleSimulationSensitive pss)
                    pss.OnChangeSimulation(simulation);
                
                content.OnAddToSpace(space);
                space.onAddItem.Invoke(content);
                content.enabled = true;
                content.body?.transform.SetParent(root);
            }
        }

        public void RemoveContent(LevelContent content) {
            if (content.space == space) {
                content.enabled = false;
                content.OnRemoveFromSpace(space);
                fieldContext.Remove(content);
                content.space = null;
                content.field = null;
            }
        }

        public override void OnRemoveFromSpace(Space space) {
            base.OnRemoveFromSpace(space);
            fieldContext.Destroy();
            root?.Destroy();
        }
        
        public IEnumerator Hiding() {
            for (var t = 0f; t < 1; t += time.Delta) {
                root.transform.position = new Vector3(
                    -10f * t.Ease(EasingFunctions.Easing.InCubic), 0, 0);
                yield return null;
            }
            root.transform.position = new Vector3(-10f, 0, 0);
        }
        
        public IEnumerator Showing() {
            for (var t = 0f; t < 1; t += time.Delta) {
                root.transform.position = new Vector3(
                    10f * (1f - t).Ease(EasingFunctions.Easing.OutCubic), 0, 0);
                yield return null;
            }
            
            root.transform.position = Vector3.zero;
        }

        public void Explode(Vector2 center, ExplosionParameters parameters) {
            if (!simulation.AllowEffects()) 
                return;
            
            var radius = parameters.radius * Slot.Offset;
            
            foreach (var slot in slots.all.Values) {
                if (!(slot.GetCurrentContent() is Chip chip)) 
                    continue;
                if ((chip.position - center).MagnitudeIsGreaterThan(radius))
                    continue;
                var impuls = (chip.position - center) * parameters.force;
                impuls *= Mathf.Pow((radius - (chip.position - center).magnitude) / radius, 2);
                chip.AddImpuls(impuls);
            }
        }

        public void Highlight(Slot[] slots, Slot center) { 
            Highlight(slots, center, ItemColorInfo.None);
        }

        public void Highlight(Slot[] slots, Slot center, ItemColorInfo colorInfo) {
            if (!simulation.AllowEffects()) 
                return;
            if (gameplay.slotHighlighter.IsNullOrEmpty())
                return;
            Effect.Emit(this, gameplay.slotHighlighter, center.position, new SlotHighlighterLogicProvider.Callback {
                slots = slots,
                center = center,
                colorInfo = colorInfo
            });
        }
        
        public IEnumerator Feedback(LocalizedText text) {
            var label = ObjectTag.Get<TMP_Text>("Feedback");

            if (!label) yield break;
            
            label.text = text.GetText();

            if (label.SetupComponent(out ContentAnimator animator))
                yield return animator.PlayAndWait("Show");
            else
                yield return time.Wait(1);
        }
        
        public bool IsInteractable(Slot slot) {
            return interactionLayer?.Contains(level, slot.coordinate) ?? true;
        }

        #region Simulation

        
        PuzzleSimulation simulation;

        public void SetSimulation(PuzzleSimulation simulation) {
            this.simulation = simulation;
            fieldContext
                .GetAll<IPuzzleSimulationSensitive>()
                .ForEach(pps => pps.OnChangeSimulation(simulation));
        }

        #endregion
        
        #region ILiveContextHolder
        
        public LiveContext GetContext() {
            return fieldContext;
        }

        #endregion
    }
    
    public interface IPuzzleSimulationSensitive : ILiveContexted {
        void OnChangeSimulation(PuzzleSimulation simulation);
    }
}