using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Yurowm;
using Yurowm.ContentManager;
using Yurowm.Coroutines;
using Yurowm.Effects;
using Yurowm.Extensions;
using Yurowm.Localizations;
using Yurowm.Serialization;
using Yurowm.Spaces;
using Yurowm.Utilities;
using Space = Yurowm.Spaces.Space;

namespace YMatchThree.Core {
    public abstract class LevelContent : SpacePhysicalItem, ISerializableID, IPuzzleSimulationSensitive {
        
        #region Storage
        [PreloadStorage]
        public static readonly Storage<LevelContent> storage = 
            new Storage<LevelContent>("LevelContent", TextCatalog.StreamingAssets);

        public static C GetItem<C>(string ID) where C : LevelContent {
            return storage.CastIfPossible<C>().FirstOrDefault(c => c.ID == ID);
        }
        
        public static C GetItem<C>() where C : LevelContent {
            return storage.CastIfPossible<C>().FirstOrDefault();
        }
        
        [LocalizationKeysProvider]
        static IEnumerable GetKeys() {
            return storage.items.CastIfPossible<ILocalized>();
        }
        
        #endregion
        
        public string ID {get; set;}
        
        public ContentAnimator animator;
        public ContentSound sound;
        
        protected LevelEvents events;
        protected LevelGameplay gameplay;

        public Field field;
        
        public bool visible => enabled;
        
        public override void OnAddToSpace(Space space) {
            events = context.GetArgument<LevelEvents>();
            
            gameplay = context.Get<LevelGameplay>();
            base.OnAddToSpace(space);
        }
        
        public void Hide() {
            if (!enabled) return;
            
            if (simulation.AllowAnimations() && animator && animator.HasClip("Hide")) {
                animator.PlayAndWait("Hide")
                    .ContinueWith(() => enabled = false)
                    .Run(field.coroutine);
            } else 
                enabled = false;
            
        }
        
        public virtual void HideAndKill() {
            if (simulation.AllowAnimations() &&animator && animator.HasClip("Hide")) {
                animator.PlayAndWait("Hide")
                    .ContinueWith(Kill)
                    .Run(field.coroutine);
            } else 
                Kill();
        }
        
        public virtual IEnumerator HidingAndKill() {
            yield return Hiding();
            Kill();
        }
        public virtual IEnumerator Hiding() {
            if (simulation.AllowAnimations() && animator && animator.HasClip("Hide"))
                yield return animator.PlayAndWait("Hide");
            enabled = false;
        }
        
        public virtual void Show() {
            enabled = true;
            if (simulation.AllowAnimations())
                animator?.Play("Awake");
        }

        public override void OnEnable() {
            base.OnEnable();
            if (!body) return;
            
            if (body.SetupComponent(out animator)) {
                if (gameplay.IsPlaying() && simulation.AllowAnimations())
                    animator.Play("Awake");
                else
                    animator.RewindEnd("Awake");

                if (simulation.AllowAnimations() && animator.HasClip("Blink"))
                    Blinking().Run(field.coroutine);
            }
                
            if (simulation != null && simulation.AllowSounds())
                if (body.SetupComponent(out sound) && gameplay.IsPlaying())
                    sound.Play("Awake");
        }

        public override void OnKill() {
            field?.RemoveContent(this);
            base.OnKill();
        }

        public abstract Type GetContentBaseType();
        
        IEnumerator Blinking() {
            while (IsAlive() && enabled && simulation.AllowAnimations()) {
                if (!animator.IsPlaying())
                    yield return PlayClipAndWait("Blink");
                    
                yield return time?.Wait(YRandom.main.Range(2f, 6f));
            }    
        }
        
        #region Variables

        public IEnumerable<Type> GetVariables() {
            var types = GetVariblesTypes().Collect<Type>();
            while (types.MoveNext())
                yield return types.Current;
        }
        
        public IEnumerable<ContentInfoVariable> EmitVariables() {
            return GetVariables()
                .Select(Activator.CreateInstance)
                .CastIfPossible<ContentInfoVariable>();
        }
        
        public virtual IEnumerator GetVariblesTypes() {
            yield break;
        }
        
        public virtual void SetupVariable(ISerializable variable) {}
        
        public void ApplyDesign(ContentInfo design) {
            design.Variables().ForEach(SetupVariable);
        }
        #endregion

        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("ID", ID);
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            ID = reader.Read<string>("ID");
        }

        #endregion
        
        #region Clips

        public void PlaySound(string clipName) {
            if (simulation.AllowSounds() && sound)
                sound.Play(clipName);
        }

        public void PlayClip(string clipName, WrapMode loop) {
            PlaySound(clipName);
            if (simulation.AllowAnimations())
                animator?.Play(clipName, loop);
        }

        public void PlayClip(string clipName) {
            PlaySound(clipName);
            if (simulation.AllowAnimations())
                animator?.Play(clipName);
        }

        public IEnumerator PlayClipAndWait(string clipName) {
            PlaySound(clipName);
            if (simulation.AllowAnimations() && animator)
                yield return animator.PlayAndWait(clipName);
        }

        #endregion
        
        #region IPuzzleSimulationSensitive

        protected PuzzleSimulation simulation;
        
        public void OnChangeSimulation(PuzzleSimulation simulation) { 
            this.simulation = simulation;
        }

        #endregion
        
        public static void VibrateWithPower(float power) {
            Vibrator.AndroidVibrate(.02f);
            Vibrator.iOSVibrate(power <.7f ? Vibrator.iOSVibrateType.Pop : Vibrator.iOSVibrateType.Peek);
        }
        
        public void ShowScoreEffect(int points, ItemColorInfo colorInfo) {
            if (gameplay.scoreEffect.IsNullOrEmpty()) return;
            
            var effect = Effect.Emit(field, gameplay.scoreEffect, position);
            
            if (effect.body.SetupChildComponent(out TextMeshPro label)) {
                label.text = points.ToString();
                label.color = colorInfo.type == ItemColor.Known ? gameplay.colorPalette.Get(colorInfo.ID) : Color.white;
            }
        }
    }
}