using System;
using System.Collections;
using Yurowm.Core;
using Yurowm.Coroutines;
using Yurowm.DebugTools;
using Yurowm.Extensions;
using Yurowm.Integrations;
using Yurowm.Serialization;
using Yurowm.Services;

namespace Yurowm.Features {
    public class LifeSystem : Integration {
        
        public int lifeCount = 5;
        public TimeSpan lifeRecoveryTimeSpan = new TimeSpan(0, 20, 0);
        
        ITimeProvider timeProvider;
        public LifeSystemData data;
        
        IEnumerator recovering;

        public bool IsRecovering => data.count < lifeCount && data.IsRecovering;

        public override string GetName() {
            return "Life System";
        }

        public override void Initialize() {
            base.Initialize();
            
            timeProvider = (ITimeProvider) Get<TrueTime>() ?? new SystemTimeProvider();
            
            data = App.data.GetModule<LifeSystemData>();
            if (data.count < 0) 
                SetFull();
            
            if (data.lifeLock) 
                BurnLife();
            
            if (data.count < lifeCount) {
                recovering = LifeRecovering();
                recovering.Run();
            }
            
            #if DEBUG
            DebugLogic().Run();
            #endif
        }

        public void SetFull() {
            data.count = lifeCount;
            data.lastBurn = default;
            data.SetDirty();
        }
        
        public void AddLife(int count = 1) {
            if (count < 1)
                return;
            
            data.count += count;
            
            if (data.count >= lifeCount) 
                data.lastBurn = default;
            
            data.SetDirty();
        }

        public void LockLife() {
            data.lifeLock = true;
            data.SetDirty();
        }

        public void UnlockLife() {
            data.lifeLock = false;
            data.SetDirty();
        }

        public bool HasLife() {
            return HasInfiniteLife() || data.count > 0;
        }

        public bool HasInfiniteLife() {
            return GetInfiniteLifeTime().Ticks > 0;
        }

        public TimeSpan GetInfiniteLifeTime() {
            if (!timeProvider.HasTime)
                return TimeSpan.Zero;
            return data.infiniteLifeEnd - timeProvider.UTCNow;
        }
        
        public void BurnLife() {
            if (!data.lifeLock) return;
            
            if (!HasLife()) return;
            
            if (!HasInfiniteLife()) 
                data.count --;
            data.lifeLock = false;
            data.SetDirty();
            
            if (recovering == null) {
                recovering = LifeRecovering();
                recovering.Run();
            }
        }

        public TimeSpan GetRecoveringTime() {
            if (!data.IsRecovering || !timeProvider.HasTime) 
                return TimeSpan.Zero;
            
            return data.lastBurn + lifeRecoveryTimeSpan - timeProvider.UTCNow;
        }

        public float GetRecoveringProgress() {
            if (!data.IsRecovering || !timeProvider.HasTime) 
                return 0;
            
            var progress = lifeRecoveryTimeSpan.TotalSeconds;
            
            progress = (progress - GetRecoveringTime().TotalSeconds) / progress;
            
            return (float) progress;
        }

        IEnumerator LifeRecovering() {
            yield return WaitForTime();
            
            while (data.count < lifeCount) {

                if (!data.IsRecovering) {
                    if (data.count < lifeCount) {
                        data.lastBurn = timeProvider.UTCNow;
                        data.SetDirty();
                    } else
                        break;
                }

                while (data.lastBurn + lifeRecoveryTimeSpan <= timeProvider.UTCNow) {
                    data.SetDirty();
                    if (data.count < lifeCount) 
                        data.count++;
                    if (data.count < lifeCount) 
                        data.lastBurn += lifeRecoveryTimeSpan;
                    else {
                        data.lastBurn = default;
                        break;
                    }
                }
                
                if (data.count < lifeCount) {
                    yield return new WaitTimeSpan(1f);
                    yield return WaitForTime();
                } else
                    break;
            }

            data.lastBurn = default;
            data.SetDirty();
            
            recovering = null;
        }
        
        IEnumerator WaitForTime() {
            while (!timeProvider.HasTime)
                yield return null;
        }
        
        IEnumerator DebugLogic() {
            DebugPanel.AddDelegate("Recover Lives", SetFull);
            DebugPanel.AddDelegate("Add Infinite Life", () => AddInfiniteLife(TimeSpan.FromMinutes(1)));
            
            while (true) {

                if (!data.IsRecovering)
                    DebugPanel.Log("Life Recovering", "Lives", "No");
                else
                    DebugPanel.Log("Life Recovering", "Lives", timeProvider.HasTime ?
                        (data.lastBurn + lifeRecoveryTimeSpan - timeProvider.UTCNow).ToString(@"hh\:mm\:ss") : "Wait");

                DebugPanel.Log("Life Count", "Lives", $"{data.count}/{lifeCount}");
                DebugPanel.Log("Life Lock", "Lives", data.lifeLock);
                
                yield return new WaitTimeSpan(1f);
            }
        }

        public void AddInfiniteLife(TimeSpan timeSpan) {
            if (!timeProvider.HasTime || timeSpan.Ticks <= 0)
                return;
            if (data.infiniteLifeEnd < timeProvider.UTCNow)
                data.infiniteLifeEnd = timeProvider.UTCNow;
            data.infiniteLifeEnd += timeSpan;
            data.SetDirty();
        }
        
        public static string TimeSpanToString(TimeSpan timeSpan) {
            var value = (int) timeSpan.TotalHours;
            
            string result;
            
            if (value > 0)
                result = value + ":";
            else 
                result = string.Empty;
            
            result += $"{timeSpan.Minutes:D2}:{timeSpan.Seconds.ClampMin(1):D2}";
            
            return result;
        }

        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("lifeCount", lifeCount);
            writer.Write("lifeRecoveryTimeSpan", lifeRecoveryTimeSpan);
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("lifeCount", out lifeCount);
            reader.Read("lifeRecoveryTimeSpan", out lifeRecoveryTimeSpan);
        }

        #endregion
    }
        
    public class LifeSystemData : GameData.Module {
        public int count = -1;
        public bool lifeLock = false;
        public DateTime lastBurn;
        public DateTime infiniteLifeEnd;
        
        public bool IsRecovering => lastBurn.Ticks != 0;

        public override void Serialize(Writer writer) {
            writer.Write("count", count);
            writer.Write("lifeLock", lifeLock);
            writer.Write("lastBurn", lastBurn);
            writer.Write("infiniteLifeEnd", infiniteLifeEnd);
        }

        public override void Deserialize(Reader reader) {
            reader.Read("count", out count);
            reader.Read("lifeLock", out lifeLock);
            reader.Read("lastBurn", out lastBurn);
            reader.Read("infiniteLifeEnd", out infiniteLifeEnd);
        }
    }
}