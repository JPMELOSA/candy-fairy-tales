using Yurowm.ObjectEditors;
using UnityEditor;
using YMatchThree.Core;
using Yurowm.Shapes;
using Yurowm.Spaces;

namespace Yurowm.YPlanets.Editor {
    public class MatchChainGameplayEditor : ObjectEditor<MatchChainGameplay> {
        public override void OnGUI(MatchChainGameplay gameplay, object context = null) {
            BaseTypesEditor.SelectAsset<YLine2D>("Line", gameplay, nameof(gameplay.lineName));
            
            gameplay.superMatchStackSize = EditorGUILayout
                .IntField("Super Match Stack Size", gameplay.superMatchStackSize)
                .ClampMin(2);
        }
    }
}