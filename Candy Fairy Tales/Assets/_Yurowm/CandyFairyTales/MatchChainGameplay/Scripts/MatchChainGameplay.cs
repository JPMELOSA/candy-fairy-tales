using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Yurowm.ContentManager;
using Yurowm.Controls;
using Yurowm.Coroutines;
using Yurowm.Extensions;
using Yurowm.Serialization;
using Yurowm.Shapes;
using Yurowm.Spaces;
using Yurowm.Utilities;
using Object = UnityEngine.Object;
using Space = Yurowm.Spaces.Space;

namespace YMatchThree.Core {
    public class MatchChainGameplay : LevelGameplay {
        
        public int superMatchStackSize = 7;
        
        List<Slot> slotStack = new List<Slot>();
        ItemColorInfo stackColor = ItemColorInfo.None;
        bool stackFilling = false;
        
        #region Events

        LevelEvents events;
        
        public Action onStackCountChanged = delegate { };
        public Action onStartFillingStack = delegate { };
        public Action onSuperMatch = delegate { };
        public Action<Slot[]> onReleaseStack = delegate { };

        #endregion

        public override void OnAddToSpace(Space space) {
            base.OnAddToSpace(space);
            
            line = AssetManager.Create<YLine2D>(lineName);
            line?.transform.SetParent(space.root);
            
            events = context.GetArgument<LevelEvents>();
            
            events.onAllGoalsAreReached += OnAllTargetsIsReached;
            
            SetupControls();
        }

        public override void OnRemoveFromSpace(Space space) {
            base.OnRemoveFromSpace(space);
            
            if (line)
                Object.Destroy(line.gameObject);
        }

        void OnStackSizeChanged() {
            onStackCountChanged?.Invoke();
            
            if (slotStack.Count >= superMatchStackSize) {
                foreach (var slot in slots.all.Values)
                    if (!slotStack.Contains(slot) && slot.GetCurrentColor().IsMatchWith(stackColor))
                        slot.OnPress();
            } else {
                foreach (var slot in slots.all.Values)
                    if (!slotStack.Contains(slot))
                        slot.OnUnpress();
            }
        }
        
        void OnAllTargetsIsReached() {
            
        }

        public override ColorBake GetColorBake() {
            return new LazyColorBake();
        }

        static readonly Side[] sidesToCheck = {
            Side.Right,
            Side.Bottom
        };
        
        public override bool IsThereAnyPotentialTurns() {
            foreach (Slot slot in slots.all.Values)
                if (slot.GetCurrentContent() is IColored coloredA)
                    foreach (var side in sidesToCheck) {
                        var next = slot[side];
                        
                        if (!next)
                            continue;

                        if (next.GetCurrentContent() is IColored coloredB)
                            if (coloredA.colorInfo.IsMatchWith(coloredB.colorInfo))
                                return true;
                    }
            
            return false;
        }

        public override bool IsThereAnySolutions() {
            return false;
        }
        
        void SetupControls() {
            Slot GetSlot(Vector2 position) {
                return space.clickables.Cast<Slot>(position, Slot.Offset);
            }
            
            void OnTouchBegin(TouchStory touch) {
                if (touch.IsOverUI) 
                    return;
                
                if (!IsPlaying()) 
                    return;
                
                if (!IsWaitingForPlayerTurn() || IsLevelComplete()) 
                    return;
                
                if (!GetSlot(touch.currentWorldPosition)) 
                    return;
                
                slotStack.Clear();
                stackFilling = true;
                stackColor = ItemColorInfo.Unknown;
                line?.Clear();

                onStartFillingStack.Invoke();
            }
            
            void OnTouch(TouchStory touch) {
                if (!stackFilling) return;
                
                if (touch.IsOverUI) return;
                
                var point = touch.currentWorldPosition;
                
                if (!slotStack.IsEmpty()) {
                    var lastSlot = slotStack.Last();
                    var delta = point - lastSlot.position;
                    if (delta.MagnitudeIsGreaterThan(Slot.Offset))
                        point = lastSlot.position + delta.normalized * Slot.Offset;
                }
                
                var slot = GetSlot(point);
                
                if (slot) AddSlotToStack(slot);
            }
            
            void OnTouchEnd(TouchStory touch) {
                if (!stackFilling) return;
                
                onReleaseStack.Invoke(slotStack.ToArray());
                gameplay.NextTask<MatchingTask>();
            }

            space.BlindCatch<CameraOperator>(o => {
                o.controls.onTouchBegin = OnTouchBegin;
                o.controls.onTouch = OnTouch;
                o.controls.onTouchEnd = OnTouchEnd;
            });
        }
        
        void AddSlotToStack(Slot slot) {
            if (!stackFilling) return;
            
            var color = slot.GetCurrentColor();

            if (!color.IsMatchableColor())
                return;

            if (slotStack.IsEmpty()) {
                slotStack.Add(slot);
                stackColor = color;
                slot.OnPress();
                OnStackSizeChanged();
                UpdateStackLine();
                return;
            }
            if (stackColor.IsMatchWith(color) && !slotStack.Contains(slot)) {
                var lastSlot = slotStack.Last();
                foreach (var side in Sides.straight)
                    if (slot[side] && slot[side] == lastSlot) {
                        slotStack.Add(slot);
                        if (!stackColor.IsKnown())
                            if (stackColor != color) {
                                stackColor = color;
                                foreach (var s in slots.all.Values)
                                    if (s.isPressed && !slotStack.Contains(s))
                                        s.OnUnpress();
                            }
                        
                        slot.OnPress();
                        OnStackSizeChanged();
                        break;
                    }
            }

            if (slotStack.Count >= 2 && slotStack[slotStack.Count - 2] == slot) {
                slotStack.RemoveAt(slotStack.Count - 1);

                var s = slotStack.Find(x => x.GetCurrentColor().type != ItemColor.Universal);

                if (s)
                    stackColor = s.GetCurrentColor();
                else
                    stackColor = ItemColorInfo.Universal;

                OnStackSizeChanged();
            }

            line?.gameObject.Repaint(colorPalette, stackColor);
            UpdateStackLine();
        }
        
        public override IEnumerator Matching() {
            stackFilling = false;
            
            if (slotStack.Count < 2) {
                foreach (var slot in slots.all.Values)
                    slot.OnUnpress();
                slotStack.Clear();
                UpdateStackLine();
                yield break;
            }

            if (!AllowToMove())
                yield break;

            OnNextTurn();

            var pressedSlots = slots.all.Values.Where(x => x.isPressed).ToArray();
            
            // var combo = combinations.FirstOrDefault(c => c.IsSuitable(pressedlist));
            
            if (superMatchStackSize > 2 &&  pressedSlots.Length >= superMatchStackSize) {
                onSuperMatch.Invoke();
                var hitContext = new HitContext(context, pressedSlots, HitReason.Matching);
                pressedSlots.ForEach(s => s.HitAndScore(hitContext));
            } else {
                var hitContext = new HitContext(context, slotStack.ToArray(), HitReason.Matching);
                for (int i = slotStack.Count - 1; i >= 0; i--) {
                    slotStack[i].HitAndScore(hitContext);
                    slotStack.RemoveAt(i);
                    UpdateStackLine();
                    
                    yield return time.Wait(0.05f);
                }
            }
            
            matchDate++;
            slotStack.Clear();
            UpdateStackLine();
            // if (combo != null) {
            //     if (bombEmittionEffect) {
            //         IChip target = GetAll<IChip>(c => c is IDestroyable && !(c is IBomb) && c is IColored && !c.slot.block).GetRandom();
            //
            //         if (target) {
            //             CollectionEffect effect = Content.Emit(bombEmittionEffect);
            //             effect.transform.SetParent(FieldAssistant.main.sceneFolder);
            //             effect.transform.Reset();
            //             effect.transform.position = ObjectTag.GetFirst<Transform>("BottomDeep").position;
            //             effect.SetItem(combo.bomb, stackColor);
            //             effect.SetTarget(target.slot.transform);
            //             effect.onReach += () => {
            //                 IChip.Explode(target.slot.transform.position, 4, 20);
            //                 FieldAssistant.main.Add(combo.bomb, target.slot, stackColor);
            //             };
            //             effect.Play();
            //             while (!effect.IsComplete())
            //                 yield return null;
            //         }
            //     }
            // }
            stackColor = ItemColorInfo.None;
        }

        #region Line

        public string lineName;
        
        YLine2D line;

        void UpdateStackLine() {
            if (!line) return;
            line.Clear();
            foreach (var slot in slotStack)
                line.AddPoint(slot.position);
        }
        
        #endregion
        
        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("line", lineName);
            writer.Write("superMatchStackSize", superMatchStackSize);
        }
        
        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("line", out lineName);
            reader.Read("superMatchStackSize", out superMatchStackSize);
        }

        #endregion
    }
}