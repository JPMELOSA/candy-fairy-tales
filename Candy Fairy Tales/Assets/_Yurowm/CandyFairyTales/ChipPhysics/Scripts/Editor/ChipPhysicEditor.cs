using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using YMatchThree.Core;
using Yurowm;
using Yurowm.ObjectEditors;
using Yurowm.Utilities;

namespace YMatchThree.Editor {
    public class ChipPhysicEditor : ObjectEditor<ChipPhysic> {
        public override void OnGUI(ChipPhysic physic, object context = null) {
            physic.acceleration = EditorGUILayout.FloatField("Acceleration", physic.acceleration).ClampMin(.1f);
            physic.speedInitial = EditorGUILayout.FloatField("Speed Initial", physic.speedInitial).ClampMin(0);
            physic.speedMax = EditorGUILayout.FloatField("Speed Max", physic.speedMax).ClampMin(physic.speedInitial);
            
            physic.bouncing = EditorGUILayout.Toggle("Bouncing", physic.bouncing);
            if (physic.bouncing) {
                physic.impulsMin = EditorGUILayout.FloatField("Impuls Min", physic.impulsMin).ClampMin(0.1f);
                physic.impulsMax = EditorGUILayout.FloatField("Impuls Max", physic.impulsMax).ClampMin(0.1f);
                
                Edit("Land Bouncing", physic.landBouncing);
            }
        }
    }
}