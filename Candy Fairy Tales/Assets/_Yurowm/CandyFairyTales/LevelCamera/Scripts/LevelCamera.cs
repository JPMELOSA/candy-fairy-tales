using System.Collections;
using System.Linq;
using UnityEngine;
using YMatchThree.Core;
using Yurowm.Extensions;
using Yurowm.Spaces;
using Yurowm.UI;
using Yurowm.Utilities;
using Behaviour = Yurowm.Behaviour;
using Space = Yurowm.Spaces.Space;

namespace YMatchThree {
    public class LevelCamera : SpaceCamera {
        UIAreaRect[] uiAreaRects;
        
        public bool animate;
        Field field;

        public override void OnAddToSpace(Space space) {
            base.OnAddToSpace(space);
            
            uiAreaRects = Behaviour.GetAllByID<UIAreaRect>("FieldRect").ToArray();
            
            uiAreaRects.ForEach(r => {
                r.onChanged.RemoveAllListeners();
                r.onChanged.AddListener(OnUIRectChanged);
            });
            OnUIRectChanged();
            
            smooth = 0;
        }

        public override void OnRemoveFromSpace(Space space) {
            base.OnRemoveFromSpace(space);
            uiAreaRects
                .ForEach(r => r.onChanged.RemoveListener(OnUIRectChanged));
        }

        public void SetField(Field field) {
            this.field = field;
            OnUIRectChanged();
        }
        
        void OnUIRectChanged() {
            if (!field && !animate) return;
            AlignToFieldEdge(GetFieldRect(), null);
        }

        Rect GetFieldRect() {
            return field.fieldContext
                .GetArgument<Slots>().edge;
        }
        
        void EdgeToPositionAndSize(UIAreaRect area, Rect edge, out float size, out Vector2 position) {
            if (!area) 
                area = uiAreaRects.FirstOrDefaultFiltered(
                    r => r.isActiveAndEnabled, 
                    r => true);
            
            area.UpdateParameters();

            float width = edge.width
                          * (area.screenSize.x / area.size.x) 
                          * Screen.height / Screen.width;
            float height = edge.height
                           * (area.screenSize.y / area.size.y);
            
            size = Mathf.Max(width, height) / 2;

            size = area.camSizeRange.Clamp(size);
            
            position = new Vector2(
                area.position.x / area.screenSize.x, 
                area.position.y / area.screenSize.y);
            
            position *= -2f * size;
            
            position.x *= 1f * Screen.width / Screen.height;
            
            position += edge.center;
        }
         
        public void AlignToFieldEdge(Rect edge, UIAreaRect area) {
            EdgeToPositionAndSize(area, edge, out var tsize, out var tposition);
                
            viewSizeVertical = tsize;
            position = tposition;
            onMove(position);
        }
        
        public IEnumerator MoveTo(Rect edge, UIAreaRect area, float duration, EasingFunctions.Easing easing = EasingFunctions.Easing.Linear) {
            while (animate) yield return null;
            animate = true;
            
            var startPosition = position;
            var startSize = camera.orthographicSize;
            
            EdgeToPositionAndSize(area, edge, 
                out var targetSize,
                out var targetPosition);
            
            for (float t = 0f; t < 1f; t += Time.deltaTime / duration) {
                position = Vector2.Lerp(startPosition, targetPosition, t.Ease(easing));
                camera.orthographicSize = Mathf.Lerp(startSize, targetSize, t.Ease(easing));
                onMove(position);
                yield return null;
            }

            position = targetPosition;
            camera.orthographicSize = targetSize;
            onMove(position);

            animate = false;
        }

        public IEnumerator RefreshPositionSmooth() {
            return MoveTo(GetFieldRect(), null, 1, EasingFunctions.Easing.InOutQuad);
        }
    }
}
