using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Yurowm.ContentManager;
using Yurowm.Controls;
using Yurowm.Coroutines;
using Yurowm.DebugTools;
using Yurowm.Extensions;
using Yurowm.Serialization;
using Yurowm.Spaces;
using Yurowm.Utilities;
using Space = Yurowm.Spaces.Space;

namespace YMatchThree.Core {
    public class MatchThreeGameplay : LevelGameplay {

        public ExplosionParameters swapBouncing = new ExplosionParameters();
        public ExplosionParameters matchBouncing = new ExplosionParameters();

        public override ColorBake GetColorBake() {
            return new MatchThreeColorBake();
        }

        public override void OnAddToSpace(Space space) {
            base.OnAddToSpace(space);
            SetupControls();
        }

        protected override void OnDebugPipeline() {
            base.OnDebugPipeline();
            DebugPanel.Log("Swapping", "Gameplay", swapping);
        }

        public override IEnumerator Matching() {
            var solutions = FindSolutions();
            if (solutions.Count > 0)
                yield return MatchSolutions(solutions);
        }

        // public override IEnumerator Pipeline(Mode inputMode) {
        //     switch (inputMode) {
        //         case Mode.Matching: {
        //             var solutions = FindSolutions();
        //             if (solutions.Count > 0)
        //                 yield return MatchSolutions(solutions);
        //         } break;
        //     }
        // }

        #region Controls
        
        Dictionary<int, TouchToSwap> touchToSwap = new Dictionary<int, TouchToSwap>();

        void SetupControls() {
            space.BlindCatch<CameraOperator>(o => {
                o.controls.onTouch = t => touchToSwap[t.fingerId]?.OnTouch(t);
                o.controls.onTouchBegin = t => touchToSwap.Set(t.fingerId, new TouchToSwap(this));
                o.controls.onTouchClick = TouchClick;
            });
            
            events.onStartTask += task => {
                if (task is WaitTask)
                    selectedChip = null;
            };
        }
        
        Chip selectedChip;

        void TouchClick(TouchStory touch) {
            if (IsLevelComplete() || !IsWaitingForPlayerTurn()) 
                return;
            
            Chip clickedChip = gameplay.space.clickables
                .Cast<Slot>(touch.currentWorldPosition, Slot.Offset)?
                .GetCurrentContent() as Chip;
            
            if (!clickedChip) return;
            
            if (selectedChip) {
                if (selectedChip == clickedChip) {
                    if (clickedChip is BombChipBase bomb && bomb.activateByClick)
                        clickedChip.HitAndScore(new HitContext(context, 
                            new [] {clickedChip.slotModule.Slot() },
                            HitReason.Player));
                        
                } else
                    SwapByPlayer(selectedChip.slotModule.Slot(), clickedChip.slotModule.Slot());
                
                selectedChip = null;
                return;
            }
            
            if (!selectedChip)
                selectedChip = clickedChip;
        }
        
        class TouchToSwap {
            IEnumerator logic;
            TouchStory touch;
            MatchThreeGameplay gameplay;

            public TouchToSwap(MatchThreeGameplay gameplay) {
                this.gameplay = gameplay;
                logic = ControlLogic();
            }
            
            IEnumerator ControlLogic() {
                if (!gameplay)
                    yield break;
                
                Slot pickedSlot = gameplay.space.clickables
                    .Cast<Slot>(touch.currentWorldPosition, Slot.Offset);

                if (!pickedSlot)
                    yield break;

                while (true) {
                    var offset = touch.currentWorldPosition - pickedSlot.position;
                    if (offset.MagnitudeIsGreaterThan(Slot.Offset / 2)) {
                        var side = Sides.straight.FirstOrDefault(s => Vector2.Angle(offset, s.ToVector2()) <= 45);
                        gameplay.SwapByPlayer(pickedSlot, side);
                        yield break;
                    }
                    yield return null;
                }                
            }
            
            public void OnTouch(TouchStory touch) {
                this.touch = touch;
                logic.MoveNext();
            }
        }

        #endregion

        #region Swapping
        
        public float swapDuration = 0.2f;
        public float swapOffset = 0.2f;
        
        bool swapping;
        
        List<Chip> lastSwaped = new List<Chip>();

        void SwapByPlayer(Slot slot, Side side) {
            SwapByPlayer(slot, slot[side]);
        }       
        
        void SwapByPlayer(Slot slotA, Slot slotB) {
            if (!IsLevelComplete() && IsWaitingForPlayerTurn()) 
                if (slotA && slotB)
                    SwapByPlayerRoutine(slotA, slotB).Run(field.coroutine);
        }
        
        IEnumerator SwapByPlayerRoutine(Slot a, Slot b) {
            if (!IsPlaying()) yield break;

            if (!AllowToMove()) yield break;
            
            if (!a.IsInteractable() || !b.IsInteractable()) yield break;

            if (swapping) yield break;
            if (!a || !b) yield break;
            if (a == b) yield break;
            
            if (a.coordinate.FourSideDistanceTo(b.coordinate) != 1) yield break;
            if (!a.nearSlot.GetKey(b).IsStraight() || !b.nearSlot.GetKey(a).IsStraight()) yield break;
            
            if (!(a.GetCurrentContent() is Chip chipA)) yield break;
            if (!(b.GetCurrentContent() is Chip chipB)) yield break;
            
            Pair<string> chipPair = new Pair<string>(chipA.ID, chipB.ID);
           
            swapping = true;
            
            bool success = false;
            
            chipA.lockPosition = true;
            chipB.lockPosition = true;
            
            var posA = a.position;
            var posB = b.position;

            float progress = 0;
            float time = 0;
            
            var mixInfo = mixes.FirstOrDefault(x => x.pair == chipPair);
            
            sound?.Play("SwapBegin");
            
            if (mixInfo != null) {
                
                field.Explode(posA, swapBouncing);
                
                while (progress < swapDuration) {
                    time = EasingFunctions.InOutQuad(progress / swapDuration);
            
                    chipA.position = Vector2.Lerp(posA, posB, time);
            
                    progress += Time.deltaTime;
            
                    yield return null;
                }
                
                var mix = storage.GetItem<ChipMix>(m => m.ID == mixInfo.mix).Clone();
                mix.random = random.NewRandom();
                mix.position = chipB.position;
                mix.slot = chipB.slotModule.Slot();
                mix.Prepare(context, chipB, chipA);
                
                matchDate++;
                
                field.AddContent(mix);

                success = true;
            } else {
            
                Vector2 normal = a.coordinate.X == b.coordinate.X ? Vector2.right : Vector2.up;
                
                field.Explode(posA, swapBouncing);
                
                while (progress < swapDuration) {
                    time = EasingFunctions.InOutQuad(progress / swapDuration);
                
                    chipA.position = Vector2.Lerp(posA, posB, time) + normal * (Mathf.Sin(Mathf.PI * time) * swapOffset);
                    chipB.position = Vector2.Lerp(posB, posA, time) - normal * (Mathf.Sin(Mathf.PI * time) * swapOffset);
                
                    progress += Time.deltaTime;
                
                    yield return null;
                }
                
                chipA.position = posB;
                chipB.position = posA;
                
                a.AddContent(chipB);
                b.AddContent(chipA);
                
                int count = MatchAnaliz(a)?.count ?? 0;
                count += MatchAnaliz(b)?.count ?? 0;
                
                if (count == 0) {
                    
                    sound?.Play("SwapFailed");
                    
                    field.Explode(posA, swapBouncing);
                    
                    while (progress > 0) {
                        time = EasingFunctions.InOutQuad(progress / swapDuration);
                        chipA.position = Vector2.Lerp(posA, posB, time) - normal * (Mathf.Sin(Mathf.PI * time) * swapOffset);
                        chipB.position = Vector2.Lerp(posB, posA, time) + normal * (Mathf.Sin(Mathf.PI  * time) * swapOffset);
                
                        progress -= Time.deltaTime;
                
                        yield return null;
                    }
                
                    a.position = posA;
                    b.position = posB;
                
                    b.AddContent(chipB);
                    a.AddContent(chipA);
                
                } else
                    success = true;
            }
            
            if (success) {
                OnNextTurn();
                lastSwaped.Clear();
                lastSwaped.Add(chipA);
                lastSwaped.Add(chipB);
            }
            
            chipA.lockPosition = false;
            chipB.lockPosition = false;

            swapping = false;
        }

        #endregion

        #region Turns

        public override bool IsThereAnyPotentialTurns() {
            return FindMoves().Count > 0;
        }

        public override bool IsThereAnySolutions() {
            return FindSolutions().Count > 0;
        }
        
        List<Solution> FindSolutions() {
            return slots.all.Values
                                   .Select(MatchAnaliz)
                                   .NotNull()
                                   .ToList();
        }
        
        #endregion

        #region Solutions

        Solution MatchAnaliz(Slot slot) {

            if (!slot) return null;

            SlotContent content = slot.GetCurrentContent();
            if (!content) return null;

            if (!(content is IColored colored))
                return null;

            var currentColorInfo = colored.colorInfo;

            if (currentColorInfo.type == ItemColor.Universal) { // multicolor
                var solutions = new List<Solution>();
                var colors = Sides.straight
                    .Select(s => slot[s])
                    .NotNull()
                    .Select(s => s.GetCurrentContent())
                    .CastIfPossible<IColored>()
                    .Where(c => c.colorInfo.IsKnown())
                    .Select(c => c.colorInfo);

                foreach (var color in colors) {
                    colored.colorInfo = color;
                    var solution = MatchAnaliz(slot);
                    if (solution != null) 
                        solutions.Add(solution);
                }

                colored.colorInfo = ItemColorInfo.Universal;
                return solutions.GetMax(x => x.potential);
            }

            var sides = new Dictionary<Side, List<SlotContent>>();
            var square = new List<SlotContent>();
            foreach (Side side in Sides.straight) {
                var count = 1;
                sides.Add(side, new List<SlotContent>());
                while (true) {
                    int2 key = slot.coordinate + side.ToInt2() * count;
                    if (!slots.all.TryGetValue(key, out Slot s)) break;

                    var currentContent = s.GetCurrentContent();
                    if (!currentContent) break;

                    if (currentContent is IColored icolored && icolored.colorInfo.IsMatchWith(colored.colorInfo)) {
                        sides[side].Add(currentContent);
                        count++;
                    } else
                        break;
                }
            }

            if (squaresMatch) {
                var zSqaure = new SlotContent[3];
                foreach (Side side in Sides.straight) {
                    for (int r = 0; r <= 2; r++) {
                        var key = slot.coordinate + side.Rotate(r).ToInt2();
                        if (!slots.all.TryGetValue(key, out var _slot))
                            break;

                        var currentContent = _slot.GetCurrentContent();
                        if (!currentContent)
                            break;

                        if (currentContent is IColored icolored && icolored.colorInfo.IsMatchWith(colored.colorInfo)) {
                            zSqaure[r] = currentContent;
                            if (r == 2)
                                square.AddRange(zSqaure);
                        } else 
                            break;
                    }
                }

                square = square.Distinct().ToList();
            } else
                square.Clear();

            SolutionType solutionType = 0;
            
            if (sides[Side.Right].Count + sides[Side.Left].Count >= 2)
                solutionType |= SolutionType.Horizontal;
            
            if (sides[Side.Top].Count + sides[Side.Bottom].Count >= 2)
                solutionType |= SolutionType.Vertical;
            
            if (square.Count > 0)
                solutionType |= SolutionType.Square;

            if (solutionType != 0) {

                var solutionContent = new List<SlotContent>();
                solutionContent.Add(slot.GetCurrentContent());

                if (solutionType.HasFlag(SolutionType.Horizontal)) {
                    solutionContent.AddRange(sides[Side.Right]);
                    solutionContent.AddRange(sides[Side.Left]);
                }
                
                if (solutionType.HasFlag(SolutionType.Vertical)) {
                    solutionContent.AddRange(sides[Side.Top]);
                    solutionContent.AddRange(sides[Side.Bottom]);
                }
                
                if (solutionType.HasFlag(SolutionType.Square))
                    solutionContent.AddRange(square);

                var solution = new Solution(solutionContent, context);

                solution.type = solutionType;

                solution.center = slot.coordinate;
                solution.colorInfo = currentColorInfo;

                foreach (var _ in solution.contents)
                    solution.potential += 1;
                
                return solution;
            }
            return null;
        }
        
        IEnumerator MatchSolutions(List<Solution> solutions) {
            if (!IsPlaying()) yield break;
            
            solutions.Sort((x, y) => y.potential.CompareTo(x.potential));
            
            var level = context.GetArgument<Level>();
            
            area levelArea = level.Area;

            var mask = new bool[levelArea.width, levelArea.height];

            slots.all.Values
                .Where(s => s.GetCurrentContent() is IColored)
                .ForEach(s => mask[s.coordinate.X, s.coordinate.Y] = true);
            
            var finalSolutions = new List<Solution>();

            foreach (var s in solutions) {
                var breaker = false;
                foreach (var c in s.contents) {
                    if (!mask[c.slotModule.Center.X, c.slotModule.Center.Y]) {
                        breaker = true;
                        break;
                    }
                }
                if (breaker) continue;

                finalSolutions.Add(s);

                s.contents.ForEach(c => mask[c.slotModule.Center.X, c.slotModule.Center.Y] = false);
            }

            matchDate++;
            int birthDate = matchDate;
            foreach (var solution in finalSolutions) {
                solution.bombSlot = solution.GetCenteredSlot();
                foreach (var combination in combinations) {
                    if (combination.count > solution.count) continue;
                    if (!combination.type.HasFlag(Combination.Type.Any)) {
                        
                        if (combination.type.HasFlag(Combination.Type.Square))
                            if (!squaresMatch || !solution.type.HasFlag(SolutionType.Square)) continue;
                        
                        if (combination.type.HasFlag(Combination.Type.Lined)) {
                            if (combination.type.HasFlag(Combination.Type.Horizontal) && !solution.type.HasFlag(SolutionType.Horizontal)) 
                                continue;
                            if (combination.type.HasFlag(Combination.Type.Vertical) && !solution.type.HasFlag(SolutionType.Vertical)) 
                                continue;
                            if (combination.type.HasFlag(Combination.Type.SingleLine) && 
                                !(solution.type.HasFlag(SolutionType.Vertical) ^ solution.type.HasFlag(SolutionType.Horizontal))) 
                                continue;
                        }
                    }

                    solution.bomb = storage.items.CastIfPossible<Chip>().FirstOrDefault(c => c.ID == combination.bomb);
                    
                    if (solution.bomb && !solution.bombSlot.IsDefault)
                        solution.bombSlot = solution.GetCenteredSlot(x => x.IsDefault);
                    
                    break;
                }
            }

            foreach (var solution in finalSolutions) {
                int score = 0;
                var hitContext = new HitContext(context, 
                    solution.contents.SelectMany(c => c.slotModule.Slots().Distinct()),
                    HitReason.Matching);
                foreach (var c in solution.contents) {
                    c.birthDate = -1;
                    score += c.slotModule.Slots().Sum(s => s.Hit(hitContext));
                }
                this.score.AddScore(score);
                
                events.onMatchSoultion(solution);
                solution.bombSlot.ShowScoreEffect(score, solution.colorInfo);

                field.Highlight(hitContext.group, solution.bombSlot);
            }

            if (finalSolutions.Count == 0)
                yield break;

            foreach (var solution in finalSolutions)
                if (solution.bomb) {
                    var bomb = solution.bomb.Clone();
                    bomb.birthDate = birthDate;
                    bomb.position = solution.bombSlot.position;
                    if (bomb is IColored)
                        bomb.SetupVariable(new ColoredVariable {
                            info = solution.colorInfo
                        });
                    bomb.emitType = SlotContent.EmitType.Matching;
                    field.AddContent(bomb);
                    solution.bombSlot.AddContent(bomb);
                }
            
            if (simulation.AllowAnimations())
                for (var t = 0f; t <= 1f; t += time.Delta * 2) {
                    foreach (var solution in finalSolutions)
                    foreach (var ch in solution.contents
                        .Where(c => c.IsAlive() && c.IsDefault && c.slotModule.HasSlot()))
                        ch.position = Vector2.Lerp(
                            ch.slotModule.Slot().position, 
                            solution.bombSlot.position, 
                            t.Ease(EasingFunctions.Easing.InCubic));
                    yield return null;
                }

            foreach (var solution in finalSolutions)
                field.Explode(solution.bombSlot.position, matchBouncing);
        }

        [Flags]
        public enum SolutionType {
            Horizontal = 1 << 1, // T + B + X >= 3
            Vertical = 1 << 2, //L + R + X >= 3
            Square = 1 << 3,
            Cross = Horizontal | Vertical
        }
        
        public class Solution {

            public int count => contents.Count;
            public int potential;
            public ItemColorInfo colorInfo;
            public List<SlotContent> contents;
            public Chip bomb;
            public Slot bombSlot;

            // center of solution
            public int2 center;

            public SolutionType type = 0;
            
            LiveContext context;
            
            public Solution(IEnumerable<SlotContent> contents, LiveContext context) {
                this.context = context;
                this.contents = contents.Distinct().ToList();
            }

            public Slot GetCenteredSlot(Func<SlotContent, bool> condition = null) {
                if (condition == null)
                    condition = NullCondition;
                
                context.SetupItem(out MatchThreeGameplay gameplay);
                
                SlotContent result = null;
                
                if (bomb) {
                    result = contents.FirstOrDefault(c => condition(c) && gameplay.lastSwaped.Contains(c));
                    if (result) 
                        return result.slotModule.Slot();
                }
                
                if (type.HasFlag(SolutionType.Square)) {
                    Slot slot = gameplay.slots.all[center];
                    
                    if (condition(slot.GetCurrentContent()))
                        return slot;
                    
                    var random = contents.FirstOrDefault()?.field.random;
                    
                    result = contents.Where(condition).GetRandom(random);
                        
                    if (!result) 
                        result = contents.GetRandom(random);
                        
                    return result?.slotModule.Slot();
                }

                int minDistance = int.MaxValue;
                
                foreach (SlotContent content in contents.Where(condition)) {
                    int2 coordA = content.slotModule.Slot().coordinate;
                    int distance = 0;
                    
                    foreach (SlotContent contentToCheck in contents.Where(condition)) {
                        if (content == contentToCheck) continue;
                        Slot slotB = contentToCheck.slotModule.Slot();
                        var coordB = slotB.coordinate;
                        int d = coordA.FourSideDistanceTo(coordB);
                        if (distance < d)
                            distance = d;
                    }
                    
                    if (minDistance > distance) {
                        minDistance = distance;
                        result = content;   
                    }
                }
                
                return result?.slotModule.Slot();
            }
            
            static bool NullCondition(SlotContent content) {
                return true;
            }
            
        }

        #endregion

        #region Moves

        public List<Move> FindMoves() {
            List<Move> moves = new List<Move>();

            foreach (Side asix in Utils.ForEachValues(Side.Right, Side.Top)) {
                foreach (Slot slot in slots.all.Values) {
                    if (!slot.IsInteractable()) continue;
                    
                    if (slot[asix] == null || !slot[asix].IsInteractable()) continue;
                    
                    if (!(slot.GetCurrentContent() is Chip chip) || !(slot[asix].GetCurrentContent() is Chip nextChip)) continue;

                    Move move = new Move {
                        A = slot.coordinate, 
                        B = slot[asix].coordinate
                    };

                    Pair<string> pair = new Pair<string>(chip.ID, nextChip.ID);

                    ChipMixRecipe mix = mixes.FirstOrDefault(x => x.pair == pair);
                    if (mix != null) {
                        move.potencial = 1000;
                        move.solution = new Solution(new [] { chip, nextChip }, context);
                        moves.Add(move);
                        continue;
                    }

                    if (chip is IColored chipColored && nextChip is IColored nextChipColored && chipColored.colorInfo.IsMatchWith(nextChipColored.colorInfo))
                        continue;

                    TemporarySwap(move);

                    Dictionary<Slot, Solution> solutions = new Dictionary<Slot, Solution>();

                    Slot[] cslots = { slot, slot[asix] };
                    foreach (Slot cslot in cslots) {
                        solutions.Add(cslot, null);

                        var potential = 0;
                        var solution = MatchAnaliz(cslot);
                        if (solution != null) {
                            solutions[cslot] = solution;
                            potential = solution.potential;
                        }

                        move.potencial += potential;
                    }

                    if (solutions[cslots[0]] != null && solutions[cslots[1]] != null)
                        move.solution = solutions[cslots[0]].potential > solutions[cslots[1]].potential ? solutions[cslots[0]] : solutions[cslots[1]];
                    else
                        move.solution = solutions[cslots[0]] ?? solutions[cslots[1]];

                    TemporarySwap(move);

                    if (move.potencial > 0) moves.Add(move);
                }
            }

            return moves;
        }
        
        void TemporarySwap(Move move) {
            var slotA = slots.all[move.A];
            var chipA = slotA?.GetContent<Chip>();
            if (!chipA) return;
            
            var slotB = slots.all[move.B];
            var chipB = slotB?.GetContent<Chip>();
            if (!chipB) return;

            chipA.BreakParent();
            chipB.BreakParent();
            
            slotA.AddContent(chipB);
            slotB.AddContent(chipA);
        }
        
        public class Move {
            public int2 A;
            public int2 B;

            public Solution solution;
            public int potencial;
        }

        #endregion

        #region Hints
        
        public override void ShowHint() {
            FindMoves()
                .GetRandom(random)?.solution.contents
                .ForEach(c => c?.Flashing());
        }

        #endregion

        #region Mixes
        
        public List<ChipMixRecipe> mixes = new List<ChipMixRecipe>();

        #endregion

        #region Combinations

        public bool squaresMatch = true;
        public List<Combination> combinations = new List<Combination>();

        public class Combination : ISerializable {
            public string bomb;
            public Type type;
            public int count = 4;

            [Flags]
            public enum Type {
                Lined = 1 << 0,
                Horizontal = 1 << 1 | Lined,
                Vertical = 1 << 2 | Lined,
                SingleLine = 1 << 3 | Lined,
                Cross = Horizontal | Vertical,
                Square = 1 << 4,
                Any = Lined | Square
            }

            public void Serialize(Writer writer) {
                writer.Write("bomb", bomb);
                writer.Write("count", count);
                writer.Write("ctype", type);
                
            }

            public void Deserialize(Reader reader) {
                reader.Read("bomb", out bomb);
                reader.Read("count", out count);
                reader.Read("ctype", out type);
            }
        }

        #endregion

        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("squares", squaresMatch);
            writer.Write("swapDuration", swapDuration);
            writer.Write("swapOffset", swapOffset);
            writer.Write("swapBouncing", swapBouncing);
            writer.Write("matchBouncing", matchBouncing);
            writer.Write("combinations", combinations.ToArray());
            writer.Write("mixes", mixes.ToArray());
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("squares", out squaresMatch);
            reader.Read("swapDuration", out swapDuration);
            reader.Read("swapOffset", out swapOffset);
            reader.Read("swapBouncing", out swapBouncing);
            reader.Read("matchBouncing", out matchBouncing);
            combinations = reader.ReadCollection<Combination>("combinations").ToList();
            mixes = reader.ReadCollection<ChipMixRecipe>("mixes").ToList();
        }

        #endregion
    }
}