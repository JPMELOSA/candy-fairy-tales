using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Yurowm;
using Yurowm.Colors;
using Yurowm.ContentManager;
using Yurowm.Extensions;
using Behaviour = Yurowm.Behaviour;

namespace YMatchThree.Core {
    public class LevelGoalCounter : Behaviour {
        
        public Image[] iconRenderers;
        
        public TextMeshProUGUI label;

        ContentAnimator animator;
        
        public const string completeClip = "Complete";

        public void Rollout() {
            label.text = "";
            if (this.SetupComponent(out animator))
                animator.Rewind(completeClip);
        }

        public void SetIcon(Sprite sprite) {
            iconRenderers.ForEach(ir => ir.sprite = sprite);
        }
    }
}