using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
#if UNITY_IAP
using UnityEngine.Purchasing;
#endif
using Yurowm.Colors;
using Yurowm.ComposedPages;
using Yurowm.Core;
using Yurowm.Extensions;
using Yurowm.Integrations;
using Yurowm.Localizations;
using Yurowm.Serialization;
using Yurowm.Services;

namespace Yurowm.Store {
    public class StorePackIAP : StorePack {
        public string iapID;
        
        public float savings = 0;
        
        #if UNITY_IAP
        IAP iap;
        
        public IAP GetIAP() {
            if (iap != null)
                return iap;
            
            iap = Integration.Get<UnityIAPIntegration>()?
                .GetIAP(iapID);
            
            if (iap != null)
                iap.onSuccess += OnSuccess;
                
            return iap;
        }

        void Purchase() {
            Integration.Get<UnityIAPIntegration>()?.Purchase(GetIAP());
        }
        
        void OnSuccess(IAP.PurchaseInfo purchaseInfo) {
            Redeem();
        }
        #endif
        
        public override void SetupButton(StoreItemButton button) {
            base.SetupButton(button);
           
            
            if (button.savings) {
                button.savings.SetActive(savings > 0);
                if (savings > 0 && button.savings.SetupChildComponent(out TMP_Text label))
                    label.text = Localization.content[savingsKey].FormatText((savings * 100).RoundToInt());
            }
        }

        protected override void SetupBuyButton(StoreItemButton button) {
            #if UNITY_IAP
            var iap = GetIAP();
            var purchasingSettings = GameSettings.Load().GetModule<PurchasingSettings>();

            if (iap is NonConsumableIAP && purchasingSettings.HasPurchaseSKU(iap.SKU))
                SetAction(button, Localization.content[takeKey].ColorizeUI("IAP"), Redeem);
            else
                SetAction(button, iap.Price.ColorizeUI("IAP"), OnClick);
            #else
            SetAction(button, "ERROR", () => {});
            #endif
        }

        public override bool IsOwned() {
            var progressData = App.data.GetModule<StoreData>();
            return base.IsOwned() || (!unlocks.IsEmpty() && unlocks.All(u => progressData.HasAccess(u)));
        } 
        
        public override void OnClick() {
            #if UNITY_IAP
            var progressData = App.data.GetModule<ProgressData>();
            if (unlocks.Any(u => progressData.HasAccess(u))) {
                var popup = ComposedPage.ByID("Popup");
                popup.Show(new AcceptPage(
                    Localization.content[messageKey],
                    Localization.content[okKey],
                    Purchase));
            } else
                Purchase();
            #endif
            
        }
        
        #region Localizion

        const string takeKey = "Store/take";
        const string savingsKey = "Store/savings";
        const string messageKey = "Popups/PackConflict/text";
        const string okKey = "Popups/PackConflict/ok";
        
        [LocalizationKeysProvider]
        static IEnumerator GetKeys() {
            yield return takeKey;
            yield return savingsKey;
            yield return messageKey;
            yield return okKey;
        }
        
        #endregion
        
        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("iapID", iapID);
            writer.Write("savings", savings);
        }
        
        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("iapID", out iapID);
            reader.Read("savings", out savings);
        }

        #endregion
    }
}