using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Yurowm;
using Yurowm.Colors;
using Button = Yurowm.Button;

namespace Yurowm.Store {
    public class StoreItemButton : BaseBehaviour {
        public Image icon;
        public TMP_Text title;
        public TMP_Text details;
        public TMP_Text cost;
        public Button buy;
        public TMP_Text noAction;
        public GameObject savings;
        
        public virtual void Reset() {
            if (savings)
                savings.SetActive(false);
        }
    }
}