using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Yurowm.Colors;
using Yurowm.ComposedPages;
using Yurowm.ContentManager;
using Yurowm.Core;
using Yurowm.Coroutines;
using Yurowm.Extensions;
using Yurowm.Localizations;
using Yurowm.Serialization;
using Yurowm.UI;
using Yurowm.Utilities;
using Page = Yurowm.ComposedPages.Page;

namespace Yurowm.Store {
    public abstract class StoreItem : ISerializableID, IStorageElementExtraData {
        
        [PreloadStorage]
        public static Storage<StoreItem> storage = new Storage<StoreItem>("Store", TextCatalog.StreamingAssets, true);

        public string ID {get; set;}
        public string iconName;
        public string group;
        public int order = 0;
        
        public bool alwaysAccess = false;
        public bool changeBackgroundColor = false;
        public Color backgroundColor;
        static readonly Color backgroundColorDefault = new Color(.1f, .1f, .1f, .3f);

        public StorageElementFlags storageElementFlags { get; set; }
        
        [OnLaunch(0)]
        static void InitializeOnLoad() {
            storage.ForEach(i => i.Initialize());
        }

        protected virtual void Initialize() { }

        public bool HasAccess() {
            return alwaysAccess || App.data.GetModule<StoreData>().HasAccess(ID);
        }

        public virtual void SetupButton(StoreItemButton button) {
            button.Reset();
            
            button.title.text = LocalizatedName;
            button.details.text = GetDetails();

            button.gameObject.Repaint(changeBackgroundColor ? 
                    backgroundColor : backgroundColorDefault);
            
            if (iconName.IsNullOrEmpty()) {
                button.icon.sprite = null;
                button.icon.enabled = false;                    
            } else {
                button.icon.sprite = AssetManager.GetAsset<Sprite>(iconName);
                button.icon.enabled = true;                    
            }
        }
        
        public virtual string GetDetails() => null;
        
        protected void SetAction(StoreItemButton button, string label, Action action = null) {
            if (action == null) {
                button.buy.NoAction();
                
                if (button.noAction) {
                    button.buy.gameObject.SetActive(false);
                    
                    button.noAction.gameObject.SetActive(true);
                    button.noAction.text = label;
                } else {
                    button.buy.gameObject.SetActive(true);
                    button.cost.text = label;
                }
            } else {
                button.buy.gameObject.SetActive(true);
                button.buy.SetAction(action);
                
                if (button.noAction) 
                    button.noAction.gameObject.SetActive(false);

                button.cost.text = label;
            }
        }
        
        protected virtual void SetupLockButton(StoreItemButton button) {
            SetAction(button,Localization.content[lockedLocalizationKey], 
                () => ComposedPage.ByID("Popup").Show(new UnlockPackOfferPage(ID)));
        }
        
        #region Localizion

        public const string localizationNameKeyFormat = "Store/{0}/title";
        public const string localizationDetailsKeyFormat = "Store/{0}/details";
        
        public const string lockedLocalizationKey = StoreList.localizationKeyPath + "locked";
        
        public string LocalizatedNameKey() => localizationNameKeyFormat.FormatText(ID);
        public string LocalizatedDetailsKey() => localizationDetailsKeyFormat.FormatText(ID);
        
        public string LocalizatedName => Localization.content[LocalizatedNameKey()];
        public string LocalizatedDetails => Localization.content[LocalizatedDetailsKey()];
        
        [LocalizationKeysProvider]
        static IEnumerator GetKeys() {
            yield return lockedLocalizationKey;
            
            storage.LoadFast();
            foreach (var item in storage.items) {
                yield return item.LocalizatedNameKey();
                yield return item.LocalizatedDetailsKey();
            }
        }
        
        #endregion
        
        #region ISerializable
        
        public virtual void Serialize(Writer writer) {
            writer.Write("ID", ID);
            writer.Write("group", group);
            writer.Write("icon", iconName);
            writer.Write("order", order);
            writer.Write("alwaysAccess", alwaysAccess);
            writer.Write("changeBackgroundColor", changeBackgroundColor);
            if (changeBackgroundColor)
                writer.Write("backgroundColor", backgroundColor);
            writer.Write("storageElementFlags", storageElementFlags);
        }

        public virtual void Deserialize(Reader reader) {
            ID = reader.Read<string>("ID");
            reader.Read("group", out group);
            reader.Read("icon", out iconName);
            reader.Read("order", out order);
            reader.Read("alwaysAccess", out alwaysAccess);
            reader.Read("changeBackgroundColor", out changeBackgroundColor);
            if (changeBackgroundColor)
                reader.Read("backgroundColor", out backgroundColor);
            storageElementFlags = reader.Read<StorageElementFlags>("storageElementFlags");
        }

        #endregion
    }
}