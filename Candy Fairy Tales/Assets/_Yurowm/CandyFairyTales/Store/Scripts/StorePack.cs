using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Yurowm;
using Yurowm.Analytics;
using Yurowm.Colors;
using Yurowm.ComposedPages;
using Yurowm.Core;
using Yurowm.Extensions;
using Yurowm.Localizations;
using Yurowm.Serialization;
using Yurowm.UI;

namespace Yurowm.Store {
    public abstract class StorePack : StoreItem {
        public List<string> unlocks = new List<string>();
        public List<StorePackItem> items = new List<StorePackItem>();
        
        public int offerOrder;
        public override void SetupButton(StoreItemButton button) {
            base.SetupButton(button);

            if (IsOwned())
                SetAction(button, Localization.content[ownKey]);
            else
                SetupBuyButton(button);
        }
        
        protected abstract void SetupBuyButton(StoreItemButton button);


        public override string GetDetails() {
            return LocalizatedDetails;
        }

        public void Redeem() {
            var progressData = App.data.GetModule<StoreData>();
            
            progressData.AddKeychain(ID, unlocks);

            foreach (var item in items) 
                item.Redeem();

            Analytic.Event("StorePack_Redeem", Segment.New("storeItemID", ID));

            UIRefresh.Invoke();
        }

        public abstract void OnClick();

        public virtual bool IsOwned() {
            return false;
        }
        
        public virtual IEnumerable<string> GetAccessKeys() {
            return unlocks;
        }
        
        #region Localization
        
        protected const string ownKey = "Store/own";
        
        [LocalizationKeysProvider]
        static IEnumerator GetKeys() {
            yield return ownKey;
        }

        #endregion
        
        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("offerOrder", offerOrder);
            writer.Write("unlocks", unlocks.ToArray());
            writer.Write("items", items.ToArray());
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("offerOrder", out offerOrder);
            
            unlocks.Clear();
            unlocks.AddRange(reader
                .ReadCollection<string>("unlocks")
                .Where(u => !u.IsNullOrEmpty()));
            items.Clear();
            items.AddRange(reader
                .ReadCollection<StorePackItem>("items"));
        }
        
        #endregion
    }

    public abstract class StorePackItem : ISerializable {
        
        public abstract void Redeem();
        public virtual void Serialize(Writer writer) { }

        public virtual void Deserialize(Reader reader) { }
    }
    
    public abstract class StorePackItemCount : StorePackItem {
        public int count = 100;

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("count", count);
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            reader.Read("count", out count);
        }
    }
}