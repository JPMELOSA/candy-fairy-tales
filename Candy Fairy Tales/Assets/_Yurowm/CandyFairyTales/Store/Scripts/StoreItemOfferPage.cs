using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Yurowm.ComposedPages;
using Yurowm.Extensions;
using Yurowm.UI;
using Page = Yurowm.ComposedPages.Page;

namespace Yurowm.Store {
    public abstract class StoreItemOfferPage : Page, IUIRefresh {
        string layoutName;
        
        public StoreItemOfferPage(string layoutName = nameof(StoreItemComposed)) {
            this.layoutName = layoutName;
            backgroundClose = true;
        }
        
        public abstract IEnumerable<StoreItem> GetItems();
        
        StoreItem[] items;
        
        public bool closeOnBuy = false;
        
        public override void Building() { 
            if (items == null)
                items = GetItems()
                    .NotNull()
                    .Distinct()
                    .Take(3)
                    .ToArray();
            
            if (items.IsEmpty())
                return;
            
            void SetNotActual() {
                isActual = false;
            }
            
            if (items.Length == 1) {
                var storeItem = AddElement<StoreItemComposed>(layoutName);
                if (storeItem) {
                    items[0].SetupButton(storeItem.button); 
                    if (closeOnBuy)
                        storeItem.button.buy.onClick.AddListener(SetNotActual);
                }
            } else {
                var container = AddElement<ComposedContainer>("ComposedHorizontalLayout");
                foreach (var pack in items) {
                    var storeItem = container.AddElement<StoreItemComposed>(layoutName);
                    if (!storeItem)
                        break;
                    
                    pack.SetupButton(storeItem.button);
                    if (closeOnBuy)
                        storeItem.button.buy.onClick.AddListener(SetNotActual);
                }
            }
            
        }
    }
}