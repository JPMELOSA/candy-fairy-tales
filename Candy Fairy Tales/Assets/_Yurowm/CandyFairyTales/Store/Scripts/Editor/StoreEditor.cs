using Yurowm.Dashboard;
using Yurowm.Serialization;
using Yurowm.Extensions;

namespace Yurowm.Store {
    [DashboardGroup("Content")]
    [DashboardTab("Store", "Coin")]
    public class StoreEditor : StorageEditor<StoreItem> {

        public override string GetFolderName(StoreItem content) {
            return content.group;
        }

        public override string GetItemName(StoreItem item) {
            return $"{item.order}. {item.ID}";
        }

        public override Storage<StoreItem> OpenStorage() {
            return StoreItem.storage;
        }

        protected override void Sort() {
            storage.items.Sort(i => i.order);
        }
    }
}