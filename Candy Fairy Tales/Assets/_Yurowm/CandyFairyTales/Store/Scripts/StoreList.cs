using System.Collections;
using System.Linq;
using Yurowm.Extensions;
using Yurowm.UI;

namespace Yurowm.Store {
    public class StoreList : VirtualizedScrollBase<StoreItemButton, StoreItem> {
        public string group;
        
        public StoreItemButton itemPrefab;
        
        StoreItem[] items;
        
        public override IList GetList() {
            if (items == null)
                items = StoreItem.storage
                    .Where(i => group.IsNullOrEmpty() || i.group == group)
                    .OrderBy(i => i.order)
                    .ToArray();
            
            return items;
        }

        public override StoreItemButton EmitItem() {
            return Instantiate(itemPrefab);
        }
        
        public override void UpdateItem(StoreItemButton item, StoreItem info) {
            info.SetupButton(item);
        }

        #region Localization

        public const string localizationKeyPath = "Pages/Store/";

        #endregion
    }
}