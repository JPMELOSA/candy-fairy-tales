using System;
using UnityEditor;
using UnityEngine;
using YMatchThree.Core;
using Yurowm.Dashboard;
using Yurowm.Icons;
using Yurowm.Nodes.Editor;

namespace YMatchThree.Editor {
    public class BuildLevelNodeEditor : NodeEditor<BuildLevelNode> {
        
        public override void OnNodeGUI(BuildLevelNode node, NodeSystemEditor editor = null) {
            OnParametersGUI(node, editor);
        }

        public override void OnParametersGUI(BuildLevelNode node, NodeSystemEditor editor = null) {
            if (GUILayout.Button("Edit")) {
                if (node.level == null)
                    node.level = new Level();
                
                if (editor?.window is YDashboard dashboard) {
                    var popup = EditorWindow.GetWindow<LevelLayoutEditorWindow>();
                    popup.Setup(node.level, editor);
                    dashboard.Show();
                }
            }
        }

    }
    public class LevelLayoutEditorWindow : EditorWindow {
        
        LevelLayoutEditor layoutEditor;
        
        public void Setup(Level nodeLevel, NodeSystemEditor editor) {
            if (layoutEditor == null)
                layoutEditor = new LevelLayoutEditor();
            
            layoutEditor.SetLevel(nodeLevel);
            layoutEditor.script = editor.nodeSystem as LevelScriptBase;
            layoutEditor.onGetDirty = editor.Save;
            
            layoutEditor.repaint = Repaint;   
            
            titleContent = new GUIContent("Level Layout");
        }
        public void OnGUI() {
            if (layoutEditor?.design != null)
                layoutEditor.OnGUI();
        }
    }
}