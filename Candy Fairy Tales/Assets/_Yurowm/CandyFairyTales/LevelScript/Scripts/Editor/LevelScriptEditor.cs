using UnityEditor;
using UnityEngine;
using YMatchThree.Core;
using Yurowm.Dashboard;
using Yurowm.Extensions;
using Yurowm.GUIHelpers;
using Yurowm.GUIStyles;
using Yurowm.Nodes;
using Yurowm.Nodes.Editor;
using Yurowm.ObjectEditors;
using Yurowm.Utilities;

namespace YMatchThree.Editor {
    public class LevelScriptEditor : LevelEditorBase, IDefault {

        public bool isDefault { get; set; } = true;
        
        NodeSystemEditor nodeSystemEditor;
        
        public override string GetName() {
            return "Script";
        }

        public override void OnSelectAnotherLevel(LevelScriptOrderedFile file) {
            nodeSystemEditor = null;
            
            if (file.Script != null) {
                nodeSystemEditor = new NodeSystemEditor(file.Script, context.controller.window) {
                    onSave = context.SetDirty
                };
            }
        }

        public override void OnGUI() {
            nodeSystemEditor?.OnGUI(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        }

        public override void OnActionToolbarGUI() {
            base.OnActionToolbarGUI();
            if (GUILayout.Button("Parameters", EditorStyles.toolbarButton, GUILayout.Width(100))) {
                if (context.controller.window is YDashboard dashboard){
                    var popup = DashboardPopup.Create<ParamtersPopup>();
                    popup.script = context.design;
                    popup.editor = nodeSystemEditor;
                    dashboard.ShowPopup(popup);
                }
            }
        }
        
        public static string NewID() {
            return YRandom.main.GenerateKey(16);
        }
        
        class ParamtersPopup : DashboardPopup {
            public LevelScriptBase script;
            public NodeSystemEditor editor;

            public override Vector2 GetSize() {
                return new Vector2(300, 400);
            }

            public override void OnGUI() {
                if (GUILayout.Button("X", Styles.centeredMiniLabelBlack, GUILayout.Width(20)))
                    ClosePopup();
                using (GUIHelper.Change.Start(editor.SetDirty))
                    ObjectEditor.Edit(script, editor);
            }
        }
    }
}