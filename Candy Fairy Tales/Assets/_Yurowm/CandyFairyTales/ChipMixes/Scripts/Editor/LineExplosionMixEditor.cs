using UnityEditor;
using YMatchThree.Core;
using Yurowm.ObjectEditors;
using Yurowm.Utilities;

namespace YMatchThree.Editor {
    public class LineExplosionMixEditor : ObjectEditor<LineExplosionMix> {
        public override void OnGUI(LineExplosionMix mix, object context = null) {
            mix.waveOffset = EditorGUILayout.IntSlider("Wave Offset", mix.waveOffset, 0, 2);
            mix.side = (Side) EditorGUILayout.EnumFlagsField("Sides", mix.side);
            Edit("Explosion", mix.explosion);
        }
    }
}