using System.Collections;
using Yurowm.ContentManager;

namespace YMatchThree.Core {
    public class MixableChipMix : ChipMix {
        IMixableChip mixable;
        Chip center;
        Chip second;

        public override void Prepare(LiveContext context, Chip chipA, Chip chipB) {
            IMixableChip mixableA = chipA as IMixableChip;
            IMixableChip mixableB = chipB as IMixableChip;

            if (mixableA != null && mixableB != null) {
                center = mixableA.MixPriority > mixableB.MixPriority ? chipA : chipB;
                second = center == chipA ? chipB : chipA;
            } else if (mixableA != null) {
                center = chipA;
                second = chipB;
            } else if (mixableB != null) {
                center = chipB;
                second = chipA;
            } else {
                UnityEngine.Debug.LogError("At least one of the chips must be mixable with the second one");
                return;
            }
            
            mixable = center as IMixableChip;
            
            if (context.SetupItem(out Score score)) {
                var points = 0;
                if (chipA is IDestroyable d1) points += d1.scoreReward;
                if (chipB is IDestroyable d2) points += d2.scoreReward;
                score.AddScore(points);
            }
            
            ApplyColor(center, second);
            
            mixable.PrepareMixWith(second);
            second.HideAndKill();
        }

        public override IEnumerator Logic() {
            if (mixable != null)
                yield return mixable.MixLogic();
            if (center.IsAlive() && !center.destroying)
                center.HideAndKill();
        }
    }
    
    public interface IMixableChip {
        int MixPriority {get;}

        void PrepareMixWith(Chip secondChip);

        IEnumerator MixLogic();
    }
}