using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using YMatchThree.Core;
using Yurowm;
using Yurowm.Extensions;
using Yurowm.HierarchyLists;
using Yurowm.Serialization;

namespace YMatchThree.Editor {
    public class LevelDesignFileWorld {
        public string name;
        static int Indexer;
        public readonly int Index;
        public string lastSelectedLevelID = null;

        public List<LevelScriptOrderedFile> files = new List<LevelScriptOrderedFile>();
            
        public LevelDesignFileWorld(string name) {
            this.name = name;
            Index = Indexer ++;
        }
    }

    public class WorldList : NonHierarchyList<LevelDesignFileWorld> {
        LevelEditorContext context;
        
        public WorldList(List<LevelDesignFileWorld> collection, LevelEditorContext context) : base(collection) {
            this.context = context;
            onRemove += list => {
                foreach (IInfo info in list) {
                    info.asItemKind.content.files.ForEach(f => f.file.Delete());
                }
                if (list.Any(i => i.asItemKind.content == context.currentWorld)) {
                    var select = itemCollection.FirstOrDefault(i => list.All(s => s.asItemKind.content != i));
                    context.controller.SelectWorld(select);
                }
            };
        }

        public override int GetUniqueID(LevelDesignFileWorld element) {
            return element.Index;
        }

        public override void DrawItem(Rect rect, ItemInfo info) {
            if (context.currentWorld == info.content)
                Highlight(rect, true);
            rect = ItemIconDrawer.Draw(rect, LevelEditorStyles.listWorldIcon);
            base.DrawItem(rect, info);
        }
        
        
        Color hightlightFace = new Color(0, 1, 1, 0.05f);
        void Highlight(Rect rect, bool outline = false) {
            rect.x += 1;
            rect.width -= 2;
            Handles.DrawSolidRectangleWithOutline(rect, hightlightFace, outline ? Color.cyan : Color.clear);
        }
        
        public override LevelDesignFileWorld CreateItem() {
            return new LevelDesignFileWorld("");
        }

        public override string GetName(LevelDesignFileWorld element) {
            return element.name;
        }

        public override void SetName(LevelDesignFileWorld element, string name) {
            element.name = name;
        }

        public override string GetLabel(ItemInfo info) {
            return $"{info.content.name} ({info.content.files.Count})";
        }

        protected override bool CanRename(TreeViewItem item) {
            return true;
        }

        protected override void RenameEnded(RenameEndedArgs args) {
            base.RenameEnded(args);
            LevelDesignFileWorld world = itemCollection.FirstOrDefault(w => GetUniqueID(w) == args.itemID);
            if (world == null) return;
            
            context.folders.RenameWorld(world.name, args.newName);
                
            world.name = args.newName;
            world.files.ForEach(w => w.World = world.name);
        }

        protected override bool CanStartDrag(CanStartDragArgs args) {
            return false;
        }
    }
}