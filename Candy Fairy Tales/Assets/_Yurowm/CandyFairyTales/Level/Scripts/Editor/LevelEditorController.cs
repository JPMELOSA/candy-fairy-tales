using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using YMatchThree.Core;
using Yurowm;
using Yurowm.ContentManager;
using Yurowm.Dashboard;
using Yurowm.GUIHelpers;
using Yurowm.GUIStyles;
using Yurowm.Utilities;
using Yurowm.Extensions;
using Yurowm.HierarchyLists;
using Yurowm.Icons;
using Yurowm.Nodes;
using Yurowm.Serialization;

namespace YMatchThree.Editor {
    [DashboardGroup("Content")]
    [DashboardTab("Levels", "LevelIcon", 10)]
    public class LevelEditorController : DashboardEditor {

        public static LevelEditorController instance;
        
        LevelEditorContext context;
        List<LevelEditorBase> editors = LevelEditorBase.allTypes.Select(Activator.CreateInstance)
            .Cast<LevelEditorBase>().ToList();

        PrefVariable lastSelectedEditor = new PrefVariable("Editor_lastSelectedEditor");

        GUIHelper.LayoutSplitter splitter;
        
        public override bool Initialize() {
            splitter = new GUIHelper.LayoutSplitter(OrientationLine.Horizontal, OrientationLine.Vertical, 200);
            
            context = new LevelEditorContext();
            context.controller = this;
            if (!lastSelectedEditor.String.IsNullOrEmpty())
                context.editor = editors.FirstOrDefault(e => e.GetName() == lastSelectedEditor.String);
            if (context.editor == null) context.editor = editors.FirstOrDefault(e => e is IDefault);
            if (context.editor == null) context.editor = editors.FirstOrDefault();
            context.editor?.SetContext(context);
            
            instance = this;
            
            loading = Loading();
            EditorCoroutine.GetCore().Run(loading);
            
            return true;
        }
        
        public static readonly Type[] colorSettingsTypes = Utils
            .FindInheritorTypes<LevelColorSettings>(true, false)
            .Where(t => !t.IsAbstract)
            .ToArray();

        #region Worlds

        List<LevelDesignFileWorld> worlds = new List<LevelDesignFileWorld>();
        
        const string defaultWorldName = "Default";
        
        IEnumerator loading;
        IEnumerator Loading() {
            Dictionary<string, LevelDesignFileWorld> worldsDict = new Dictionary<string, LevelDesignFileWorld>();
            
            yield return UnityUtils.ExecuteAsync(() => {
                var start = DateTime.Now;
                
                var files = LevelEditorContext.directory.GetFiles()
                    .Where(f => f.Extension == Serializator.FileExtension && f.Name.StartsWith("level_")).ToArray();
                
                for (int i = 0; i < files.Length; i++) {
                    FileInfo file = files[i];
                    
                    try {
                        var designFile = new LevelScriptOrderedFile(file);
                        
                        if (!worldsDict.ContainsKey(designFile.World))
                            worldsDict[designFile.World] = new LevelDesignFileWorld(designFile.World);
                        
                        worldsDict[designFile.World].files.Add(designFile);
                    } catch (Exception e) {
                        Debug.LogException(e);
                    }
                }

                if (worldsDict.Count == 0)
                    worldsDict.Add(defaultWorldName, new LevelDesignFileWorld(defaultWorldName));
                
                worldsDict.Values.ForEach(w => w.files = w.files.OrderBy(f => f.Order).ToList());
            });
            
            if (!EditorApplication.isCompiling) {
                
                var selectedLevelID = lastSelectedLevelID.String;
                
                var level = worlds.SelectMany(w => w.files)
                    .FirstOrDefault(l => l.ScriptPreview.ID == selectedLevelID);
                
                if (level != null && !level.World.IsNullOrEmpty())
                    worldsDict.TryGetValue(level.World, out context.currentWorld);

                if (context.currentWorld == null)
                    context.currentWorld = worldsDict.ContainsKey(defaultWorldName) ? 
                        worldsDict[defaultWorldName] : worldsDict.Values.First();

                worlds = worldsDict.Values.ToList();
                
                levelList = new LevelList(context.currentWorld.files, 
                    context.folders.Load(context.currentWorld.name).Select(p => new TreeFolder {fullPath = p}).ToList(),
                    context);
                levelList.onSelectedItemChanged += x => {
                    if (x.Count == 1 && x[0] != context.designFile)
                        SelectLevel(x[0]);
                };
                
                worldList = new WorldList(worlds, context);
                worldList.onSelectedItemChanged += x => {
                    if (x.Count > 0 && x[0] != context.currentWorld)
                        SelectWorld(x[0]);
                };

                SelectLevel(selectedLevelID);
            }
            
            loading = null;
        }
        
        #endregion
        
        #region GUI

        public override void OnGUI() {
            if (loading != null) {
                GUILayout.Box("Loading...", Styles.centeredMiniLabel, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
                Repaint();
                return;
            }
            
            if (context.design == null) levelList.visible = true;
            
            using (GUIHelper.Horizontal.Start()) {
                using (splitter.Start(levelList.visible, context.editor != null)) {
                    if (splitter.Area()) {
                        GUILayout.Label("Wolrds List", Styles.centeredMiniLabel, GUILayout.ExpandWidth(true)); 
                        worldList.OnGUI(Mathf.Min(100, worldList.totalHeight + 30));
                        
                        GUILayout.Label("Levels List", Styles.centeredMiniLabel, GUILayout.ExpandWidth(true));
                        levelList.OnGUI();
                    }

                    if (splitter.Area())
                        context.editor.OnGUI();
                }
            }
            
            if (context.designFile != null && context.designFile.dirty) {
                context.designFile.Save();
                BuildPreviewFile();
            }
        }

        #region Preview File

        LevelScriptOrderedPreviews previews = new LevelScriptOrderedPreviews();

        public void BuildPreviewFile() {
            previews.scripts = worlds
                .SelectMany(w => w.files)
                .Select(f => f.ScriptPreview)
                .ToArray();
            
            TextData.SaveText(Path.Combine(nameof(LevelScriptOrdered), $"LevelsPreview{Serializator.FileExtension}") , Serializator.ToTextData(previews)); 
        }
        
        #endregion
        
        protected static Texture2D navFirstIcon;
        protected static Texture2D navPreviousIcon;
        protected static Texture2D navNextIcon;
        protected static Texture2D navLastIcon;

        protected static Texture2D playIcon;
        
        public override void OnToolbarGUI() {
            if (loading != null) {
                base.OnToolbarGUI();
                return;
            }

            #region Icons

            {
                if (navFirstIcon == null)
                    navFirstIcon = EditorIcons.GetUnityIcon("Animation.FirstKey@2x", "d_Animation.FirstKey@2x");
                
                if (navPreviousIcon == null)
                    navPreviousIcon = EditorIcons.GetUnityIcon("Animation.PrevKey@2x", "d_Animation.PrevKey@2x");
                
                if (navNextIcon == null)
                    navNextIcon = EditorIcons.GetUnityIcon("Animation.NextKey@2x", "d_Animation.NextKey@2x");
                
                if (navLastIcon == null)
                    navLastIcon = EditorIcons.GetUnityIcon("Animation.LastKey@2x", "d_Animation.LastKey@2x");
                
                if (playIcon == null)
                    playIcon = EditorIcons.GetUnityIcon("PlayButton@2x", "d_PlayButton@2x");
            }

            #endregion
            
            #region Navigation Panel
            
            using (GUIHelper.Horizontal.Start()) {
                if (GUILayout.Button(navFirstIcon, EditorStyles.toolbarButton))
                    SelectLevel(0);
                
                if (GUILayout.Button(navPreviousIcon, EditorStyles.toolbarButton))
                    SelectLevel(levelList.itemCollection.IndexOf(context.designFile) - 1);

                levelList.visible = GUILayout.Toggle(levelList.visible, "List",
                    EditorStyles.toolbarButton, GUILayout.ExpandWidth(true));

                if (GUILayout.Button(navNextIcon, EditorStyles.toolbarButton))
                    SelectLevel(levelList.itemCollection.IndexOf(context.designFile) + 1);
                
                if (GUILayout.Button(navLastIcon, EditorStyles.toolbarButton))
                    SelectLevel(levelList.itemCollection.Count - 1);
            }
            #endregion
            
            #region Editor Selection
            using (GUIHelper.BackgroundColor.Start(Color.Lerp(Color.white, Color.yellow, 0.6f)))
                GUIHelper.Popup(null, context.editor, editors, EditorStyles.toolbarPopup, 
                    e => e.GetName(), e => {
                        context.editor = e;
                        e.SetContext(context);
                        e.OnSelectAnotherLevel(context.designFile);
                    }, e => e != null, GUILayout.Width(100));                
            #endregion
            
            GUILayout.Label($"Level #{context.design.order} ({context.design.ID})", EditorStyles.toolbarButton);
            
            GUILayout.FlexibleSpace();

            #region Action Buttons
            using (GUIHelper.Horizontal.Start()) {
                GUILayout.FlexibleSpace();
                context.editor.OnActionToolbarGUI();
                
                using (GUIHelper.Lock.Start(EditorApplication.isPlayingOrWillChangePlaymode))
                using (GUIHelper.BackgroundColor.Start(Color.Lerp(Color.white, Color.green, 0.6f)))
                    if (GUILayout.Button(playIcon, EditorStyles.toolbarButton)) 
                        Launcher.TestScript(context.design);
            }
            #endregion
        }
        
        #endregion

        #region Selection
        public void SelectWorld(LevelDesignFileWorld world) {
            if (world != null && !worlds.Contains(world)) return;
            
            if (context.currentWorld == world) return;
            
            context.currentWorld = world;
            
            levelList.itemCollection = context.currentWorld?.files ?? new List<LevelScriptOrderedFile>();
            levelList.folderCollection = context.folders.Load(world.name)
                .Select(p => new TreeFolder() {fullPath = p})
                .ToList();
            
            levelList.Reload();
            if (levelList.itemCollection.Count > 0) {
                if (world.lastSelectedLevelID.IsNullOrEmpty())
                    SelectLevel(world.files.FirstOrDefault());
                else
                    SelectLevel(world.lastSelectedLevelID);
            } else
                SelectLevel((LevelScriptOrderedFile) null);
        }
        
        public void SelectLevel(LevelScriptOrderedFile scriptFile) {
            if (scriptFile?.Script != null) {
                context.SetDesign(scriptFile);
                context.currentWorld.lastSelectedLevelID = scriptFile.Script.ID;
                
                levelList.Select(x => Equals(x, scriptFile));
            } else
                levelList.Select(null);

            context.editor?.SetContext(context);
            context.editor?.OnSelectAnotherLevel(scriptFile);

            scriptFile?.SetDirty();
            
            lastSelectedLevelID.String = scriptFile?.ScriptPreview.ID;
        }

        public void SelectLevel(string ID) {
            if (ID.IsNullOrEmpty()) return;
            
            var level = worlds.SelectMany(w => w.files)
                .FirstOrDefault(l => l.ScriptPreview.ID == ID);
            
            if (level == null) level = levelList.itemCollection.FirstOrDefault();
            
            if (level != null) {
                SelectWorld(worlds.FirstOrDefault(w => w.name == level.World));
                
                SelectLevel(level);
            }
        }
        
        public void SelectLevel(int number) {
            if (number < 0 || number >= levelList.itemCollection.Count) return;
            
            var level = levelList.itemCollection[number];
            
            if (level == null) level = levelList.itemCollection.FirstOrDefault();
            
            SelectLevel(level);
        }

        #endregion

        #region Level Selector
        public LevelList levelList;
        public WorldList worldList;

        public static PrefVariable lastSelectedLevelID = new PrefVariable("Editor_lastSelectedLevel");
        #endregion
    }
}