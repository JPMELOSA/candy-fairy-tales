using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using Yurowm.Serialization;
using Yurowm.UI;
using Space = Yurowm.Spaces.Space;

namespace YMatchThree.Core {
    public abstract class CollectionGoal : LevelGoal {
        
        public int count = 10;
        public bool all = false;
        
        public int collected = 0;
        
        public override void OnAddToSpace(Space space) {
            base.OnAddToSpace(space);
            events.onStartDestroying += OnStartDestroying;
            if (all)
                events.onStartTask += OnStartTask;
        }

        void OnStartDestroying(SlotContent content, HitContext hitContext) {
            if (!ContentFilter(content)) return;
            if (!all)
                Collect();
            UIRefresh.Invoke();
        }

        void OnStartTask(LevelGameplay.Task task) {
            if (all && task is WaitTask)
                UIRefresh.Invoke();
        }
        
        protected abstract bool ContentFilter(SlotContent content);

        void Collect() {
            if (collected < count)
                collected++;
        }

        public override bool IsFailed() {
            return false;
        }

        public override string GetCounterValue() {
            if (all)
                return context.Count<SlotContent>(ValidateExistedContent).ToString();
            else
                return (count - collected).ToString();
        }

        bool ValidateExistedContent(SlotContent content) {
            return content.slotModule.HasSlot() 
                   && (!(content is IDestroyable destroyable) || !destroyable.destroying)
                   && ContentFilter(content);
        }
        
        bool isComplete = false;
        
        public override bool IsComplete() {
            if (isComplete) return true;
            
            if (all)
                isComplete = !context.Contains<SlotContent>(ValidateExistedContent);
            else
                isComplete = collected >= count;
            
            if (isComplete) {
                events.onStartDestroying -= OnStartDestroying;
                events.onStartTask -= OnStartTask;
            }
            
            return isComplete;
        }

        public override IEnumerator GetVariblesTypes() {
            yield return base.GetVariblesTypes();
            yield return typeof(CollectionSizeVariable);
        }

        public override void SetupVariable(ISerializable variable) {
            base.SetupVariable(variable);
            switch (variable) {
                case CollectionSizeVariable size: {
                    count = size.count; 
                    all = size.all; 
                    return;
                }
            }
        }

    }
    
    public class CollectionSizeVariable : ContentInfoVariable {
        public int count = 10;
        public bool all = false;
        
        public override void Serialize(Writer writer) {
            writer.Write("colall", all);
            if (!all)
                writer.Write("colcount", count);
        }

        public override void Deserialize(Reader reader) {
            reader.Read("colall", out all);
            if (!all)
                reader.Read("colcount", out count);
        }
    }
}