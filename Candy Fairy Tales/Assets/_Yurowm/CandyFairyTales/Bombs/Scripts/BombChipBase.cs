using System.Collections;
using Yurowm.Serialization;

namespace YMatchThree.Core {
    public abstract class BombChipBase : Chip, IDestroyable {
        
        public bool activateByClick { get; set; } = false;
        
        public int scoreReward { get; set; }
        
        public abstract IEnumerator Exploding();

        public IEnumerator Destroying() {
            yield return Exploding();
        }

        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("activateByClick", activateByClick);
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader); 
            activateByClick = reader.Read<bool>("activateByClick"); 
        }

        #endregion

    }
}