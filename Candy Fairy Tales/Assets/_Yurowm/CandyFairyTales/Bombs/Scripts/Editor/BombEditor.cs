using Yurowm.ObjectEditors;
using UnityEditor;
using YMatchThree.Core;

namespace Yurowm.Editors {
    public class BombEditor : ObjectEditor<Bomb> {
        public override void OnGUI(Bomb bomb, object context = null) {
            Edit("Explosion", bomb.explosion);
        }
    }
}