using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using YMatchThree.Core;
using Yurowm;
using Yurowm.ObjectEditors;

namespace YMatchThree.Editor {
    public class BombChipBaseEditor : ObjectEditor<BombChipBase> {
        public override void OnGUI(BombChipBase chip, object context = null) {
            chip.activateByClick = EditorGUILayout.Toggle("Activate By Click", chip.activateByClick);
        }
    }
}