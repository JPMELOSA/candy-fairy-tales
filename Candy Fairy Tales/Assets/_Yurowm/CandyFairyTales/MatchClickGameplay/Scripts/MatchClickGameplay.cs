using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Yurowm.ContentManager;
using Yurowm.Controls;
using Yurowm.Coroutines;
using Yurowm.Extensions;
using Yurowm.Serialization;
using Yurowm.Spaces;
using Yurowm.Utilities;
using Space = Yurowm.Spaces.Space;

namespace YMatchThree.Core {
    public class MatchClickGameplay : LevelGameplay {

        public override ColorBake GetColorBake() {
            return new LazyColorBake();
        }

        public override void OnAddToSpace(Space space) {
            base.OnAddToSpace(space);
            SetupControls();
        }

        // public override IEnumerator Pipeline(Mode inputMode) {
        //     switch (inputMode) {
        //         case Mode.Wait: {
        //             ShowHelpers();
        //         } break;
        //         case Mode.Matching: {
        //             HideHelpers();
        //             if (!matching && pipelineLockers.IsEmpty())
        //                 yield return Mode.Reaction;
        //     
        //             yield return new WaitWithDelay(() => 
        //                 !matching && pipelineLockers.IsEmpty(), .1f);
        //     
        //             yield return Mode.Gravity;
        //         } break;
        //     }
        // }

        public override IEnumerator Matching() {
            if (!matching)
                yield break;
            
            while (matching)
                yield return null;
        }
        
        #region Hints
        
        void ShowHelpers() {
            var moves = FindMoves()
                .Select(x => x.Select(y => y.GetCurrentContent()).ToArray()).ToArray();
            foreach (var move in moves) {
                Texture2D helper = null;
                foreach (var combination in combinations)
                    if (combination.IsSuitable(move)) {
                        if (!combination.helper.IsNullOrEmpty())
                            helper = AssetManager.GetAsset<Texture2D>(combination.helper);
                        break;
                    }
                if (helper != null)
                    move.ForEach(x => ShowHelper(x, helper));
            }
        }

        void HideHelpers() {
            context.GetAll<SlotContent>().ForEach(s => ShowHelper(s, null));
        }

        void ShowHelper(SlotContent chip, Texture2D texture) {
            if (!texture) return;
            chip.body.GetComponentsInChildren<SpriteHelper>()
                .ForEach(x => x.SetTexture(texture));
        }    

        #endregion
        
        #region Controls

        void SetupControls() {
            space.BlindCatch<CameraOperator>(o => {
                o.controls.onTouch += OnTouch;
            });
        }
        
        void OnTouch(TouchStory touch) {
            if (!gameplay.IsLevelComplete() && gameplay.IsWaitingForPlayerTurn()) {
                if (touch.IsOverUI)
                    return;
                
                var slot = space.clickables
                    .Cast<Slot>(touch.currentWorldPosition, Slot.Offset);
                
                if (slot)
                    Match(slot).Run(field.coroutine);
            }
            
        }

        #endregion
        
        #region Moves

        public override bool IsThereAnyPotentialTurns() {
            foreach (var slot in slots.all.Values) {
                var color = slot.GetCurrentColor();
                if (color.IsMatchableColor())
                    foreach (Side side in Sides.straight)
                        if (slot[side] && color.IsMatchWith(slot[side].GetCurrentColor()))
                            return true;
            }
            return slots.all.Values.Any(s => s.GetCurrentContent() is BombChipBase);

        }

        public override bool IsThereAnySolutions() {
            return false;
        }
        
        List<List<Slot>> FindMoves() {
            List<List<Slot>> result = new List<List<Slot>>();
            
            var pool = slots.all.Values
                .Select(x => x.GetCurrentContent())
                .NotNull()
                .Where(c => c is IColored)
                .SelectMany(c => c.slotModule.Slots())
                .Distinct()
                .ToList();

            while (pool.Count > 0) {
                Slot first = pool.First();
                List<Slot> match = Matcher(new List<Slot>(), first);
                pool.RemoveAll(s => match.Contains(s));
                if (match.Count > 1)
                    result.Add(match);
            }

            return result;
        }

        #endregion

        #region Matching

        bool matching = false;
        
        IEnumerator Match(Slot slot) {
            if (slot.GetCurrentContent() is BombChipBase bomb) {
                var pairs = slot.nearSlot.Where(x => x.Value && x.Key.IsStraight() && x.Value.GetCurrentContent() is Chip)
                    .Select(x => x.Value.GetContent<Chip>())
                    .GroupBy(x => new Pair<string>(bomb.ID, x.ID))
                    .ToDictionary(x => x.Key, x => x.First());

                var mixInfo = mixes.FirstOrDefault(a => pairs.ContainsKey(a.pair));

                if (mixInfo != null) {
                    matching = true;
                    
                    OnNextTurn();

                    Chip chipA = pairs[mixInfo.pair];
                    Chip chipB = bomb;

                    float progress = 0;
                    const float duration = .2f;
                    
                    Vector2 posA = chipA.slotModule.Position;
                    Vector2 posB = chipB.slotModule.Position;

                    for (float t = 0; t < .2f; t += Time.deltaTime) {
                        var time = EasingFunctions.InOutQuad(progress / duration);
                        chipA.position = Vector3.Lerp(posA, posB, time);
                        yield return null;
                    }

                    var mix = storage.GetItem<ChipMix>(m => m.ID == mixInfo.mix).Clone();
                    mix.random = random.NewRandom();
                    mix.position = chipB.position;
                    mix.slot = chipB.slotModule.Slot();
                    mix.Prepare(context, chipB, chipA);

                    matchDate++;

                    field.AddContent(mix);

                    matching = false;
                } else {
                    OnNextTurn();
                    slot.HitAndScore(new HitContext(context, slot, HitReason.Matching));
                }
                
                yield break;
            }

            var area = Matcher(new List<Slot>(), slot);

            if (area.Count < 2)
                yield break;

            matching = true;

            var hitContext = new HitContext(context, area, HitReason.Matching);
            
            matchDate++;
            
            var content = area.Select(x => x.GetCurrentContent())
                .Where(c => c.IsDefault)
                .ToList();
            
            if (content.Count > 0)
                if (!slot.GetCurrentContent().IsDefault)
                    slot = content
                        .Where(c => c.slotModule is SingleSlotModule)
                        .GetRandom(random)?.slotModule.Slots().FirstOrDefault();

            for (float t = 0; t < 1; t += Time.deltaTime * 7) {
                foreach (var chip in content)
                    if (chip && chip.slotModule.HasSlot()) 
                        chip.position = Vector2.Lerp(chip.slotModule.Position, slot.position, t);
                yield return null;
            }
            
            int points = area.Sum(x => x.Hit(hitContext));
            if (points > 0) {
                score.AddScore(points);
                slot.ShowScoreEffect(points, slot.GetCurrentColor());
            }
            
            if (content.Count > 0) {
                var allContent = area.Select(x => x.GetCurrentContent()).ToList();
                foreach (var combination in combinations) {
                    if (combination.IsSuitable(allContent)) {
                        var _bomb = storage.Items<Chip>().FirstOrDefault(b => b.ID == combination.bomb)?.Clone();
                        
                        _bomb.birthDate = matchDate;
                        _bomb.position = slot.position;
                        if (_bomb is IColored)
                            _bomb.SetupVariable(new ColoredVariable {
                                info = content[0].colorInfo
                            });
                        field.AddContent(_bomb);
                        slot.AddContent(_bomb);
                        break;
                    }
                }
            }

            foreach (var chip in content)
                if (chip) 
                    chip.position = slot.position;

            OnNextTurn();
            
            matching = false;
        }
        
        List<Slot> Matcher(List<Slot> all, Slot center) {
            if (!(center.GetCurrentContent() is IColored colored))
                return all; 

            var colorInfo = colored.colorInfo;

            all.Add(center);

            foreach (var s in center.nearSlot) {
                if (s.Key.IsSlanted()) 
                    continue;
                if (s.Value == null || all.Contains(s.Value))
                    continue;
                if (s.Value.GetCurrentContent() is IColored c && c.colorInfo.IsMatchWith(colorInfo))
                    all = Matcher(all, s.Value);
            }
            
            return all;
        }

        #endregion

        #region Mixes

        public List<ChipMixRecipe> mixes = new List<ChipMixRecipe>();
        
        #endregion

        #region Combinations

        public List<Combination> combinations = new List<Combination>();

        public class Combination : ISerializable {
            public string bomb;
            
            public int count = 4;
            public int vertCount = 1;
            public int horizCount = 1;
            
            public string helper;

            public bool IsSuitable(IEnumerable<SlotContent> content) {
                int xmin = int.MaxValue;
                int ymin = int.MaxValue;
                int xmax = int.MinValue;
                int ymax = int.MinValue;
                int contentCount = 0;
                
                content.ForEach(c => {
                    xmin = Mathf.Min(xmin, c.slotModule.Center.X);
                    ymin = Mathf.Min(ymin, c.slotModule.Center.Y);
                    xmax = Mathf.Max(xmax, c.slotModule.Center.X);
                    ymax = Mathf.Max(ymax, c.slotModule.Center.Y);
                    contentCount++;
                });

                int horiz = xmax - xmin;
                int vert = ymax - ymin;

                return count <= contentCount &&
                       vertCount <= vert && horizCount <= horiz &&
                       !(vertCount != horizCount && 
                         vertCount >= horizCount != vert >= horiz);
            }
            
            public void Serialize(Writer writer) {
                writer.Write("bomb", bomb);
                writer.Write("helper", helper);
                writer.Write("count", count);
                writer.Write("vertCount", vertCount);
                writer.Write("horizCount", horizCount);
            }

            public void Deserialize(Reader reader) {
                reader.Read("bomb", out bomb);
                reader.Read("helper", out helper);
                reader.Read("count", out count);
                reader.Read("vertCount", out vertCount);
                reader.Read("horizCount", out horizCount);
            }
        }
        
        #endregion
        
        
        #region ISerializable

        public override void Serialize(Writer writer) {
            base.Serialize(writer);
            writer.Write("combinations", combinations.ToArray());
            writer.Write("mixes", mixes.ToArray());
        }

        public override void Deserialize(Reader reader) {
            base.Deserialize(reader);
            combinations = reader.ReadCollection<Combination>("combinations").ToList();
            mixes = reader.ReadCollection<ChipMixRecipe>("mixes").ToList();
        }

        #endregion
    }
}